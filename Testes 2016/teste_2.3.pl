%prato(nomePrato,tipo,preco)
prato(omolete,principal,6.5).
prato(rojoes,principal,12).
prato(caldo_verde,sopa,3.5).
prato(canja_galinha,sopa,3.5).
prato(pizza,principal,8).
prato(mousse_chocolate,sobremesa,3).
prato(crepe,sobremesa,4).
prato(tabua_queijos,entrada,8).
prato(alheira,entrada,4).
/*Pedido resumido era algo do g�nero: Construa uma ementa com base numa lista de tipos de pratos introduzida pelo utilizador, respeitando o custo m�x indicado
%exemplo:gera_ementa([principal,sopa],E,15).
.

gera_ementa(_,[],[]).
gera_ementa(Max,[(Valor1,Valor2)|LP],[(Nome1,Nome2)|E]):-
	prato(Nome1,Valor1,P1),
	prato(Nome2,Valor2,P2),
	Soma is P2 + P1,
	Soma < Max,
	gera_ementa(Max,LP,E).
gera_ementa(Max,[(Valor1,Valor2,Valor3)|LP],[(Nome1,Nome2,Nome3)|E]):-
	prato(Nome1,Valor1,P1),
	prato(Nome2,Valor2,P2),
	prato(Nome3,Valor3,P3),
	Soma is P3 + P2 + P1,
	Soma < Max,
	gera_ementa(Max,LP,E).
gera_ementa(Max,[(Valor1,Valor2,Valor3,Valor4)|LP],[(Nome1,Nome2,Nome3,Nome4)|E]):-
	prato(Nome1,Valor1,P1),
	prato(Nome2,Valor2,P2),
	prato(Nome3,Valor3,P3),
	prato(Nome4,Valor4,P4),
	Soma is P4 + P3 + P2 + P1,
	Soma < Max,
	gera_ementa(Max,LP,E).
*/

gera_ementa([],[],_).
gera_ementa([X|Y],[P|E],V):-
	prato(P,X,V1),
	N is V-V1,
	N>0,
	gera_ementa(Y,E,N).
