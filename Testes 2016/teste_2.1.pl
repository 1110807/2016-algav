/*
Escreva um predicado em Prolog que dado um or�amento m�ximo por viagem e uma lista com os destinos com voo direto a partir do Porto, respetivos pre�os e lugares dispon�veis, crie duas listas, com os destinos dentro do or�amento previsto:
- uma com os destinos para os quais h� lugares dispon�veis
- outra lista com os destinos para os quais ser� necess�rio ficar em lista de espera.
% destinoBudget (Or�amentoMax, ListaDestinosDiretos,ListaDestinosDispon�veis, ListaDestinosEspera)
Exemplo: destinos( 100, [(Paris, 110, 10), (Toulouse,70, 3), (Praga,120, 0), (Madrid,65, 5), (Berlin,90, 0)], LD, LE).
LD = [Toulouse,Madrid]
LE = [Berlim]
*/

destinos(_,[],[],[]):-!.
destinos(Max, [(NomeC,Valor,Lugares)|LV],[(NomeC)|LD], LE):-
	Max > Valor,
	Lugares > 0,!,
	destinos(Max,LV,LD,LE).
destinos(Max, [(NomeC,Valor,Lugares)|LV], LD, [(NomeC)|LE]) :-
	Max < Valor,
	Lugares = 0,!,
	destinos(Max,LV,LD,LE).
destinos(Max,[_|LV],LD,LE):-
	destinos(Max,LV,LD,LE).
