/*
destinoBudget (Or�amentoMax, ListaDestinosDiretos,ListaDestinosDispon�veis, ListaDestinosEspera)

Exemplo: destinos( 100, [(Paris, 110, 10), (Toulouse,70, 3), (Praga,120, 0), (Madrid,65, 5), (Berlin,90, 0)], LD, LE).
LD = [Toulouse,Madrid]
LE = [Berlim]
*/
destinos(_,[],[],[]).
destinos(Max,[(NomeC,Valor,Lugares)|LV],[NomeC|LD],LE):-
	Max > Valor,
	Lugares > 0,!,
	destinos(Max,LV,LD,LE).
destinos(Max,[(NomeC,Valor,Lugares)|LV],LD,[NomeC|LE]):-
	Max > Valor,
	Lugares = 0,!,
	destinos(Max,LV,LD,LE).
destinos(Max,[_|LV],LD,LE):-
	destinos(Max,LV,LD,LE).

/*
Elabore um predicado em Prolog que faz o pr�-procesamento de uma lista de valores, de modo a eliminar e registar os erros ocorridos.
Os erros s�o valores negativos, que ter�o que ser corrgidos para 0. O registo de erros deve ser feito para uma lista que indique qual o erro(valor negativo detetado)
e a sua posi��o na lista.
%preProc(ListaValores,ListaCorrigida,ListaErros).
Exemplo:
preProc([1,0,-2,4,-5,200,-2,44,-4],LC,LE).
LC = [1, 0, 0, 4, 0, 200, 0, 44, 0],
LE = [ (-2, 3), (-5, 5), (-2, 7), (-4, 9)].
*/

preProc(LV,LC,LE):-corrigir(LV,LC,LE,1),!,true.
corrigir([],[],[],_).
corrigir([Valor|LV],[0|LC],[(Valor,C)|LE],C):-
	Valor < 0,
	C1 is C + 1,
	corrigir(LV,LC,LE,C1).
corrigir([Valor|LV],[Valor|LC],LE,C):-
	C1 is C + 1,
	corrigir(LV,LC,LE,C1).


%prato(nomePrato,tipo,preco)
prato(omolete,principal,6.5).
prato(rojoes,principal,12).
prato(caldo_verde,sopa,3.5).
prato(canja_galinha,sopa,3.5).
prato(pizza,principal,8).
prato(mousse_chocolate,sobremesa,3).
prato(crepe,sobremesa,4).
prato(tabua_queijos,entrada,8).
prato(alheira,entrada,4).
/*Pedido resumido era algo do g�nero: Construa uma ementa com base numa lista de tipos de pratos introduzida pelo utilizador, respeitando o custo m�x indicado
%exemplo:gera_ementa([principal,sopa],E,15).
.*/

gera_ementa([],[],_).
gera_ementa([Produto|LV],[Nome|E],Max):-
	prato(Nome,Produto,Preco),
	Total is Max - Preco,
	Total > 0,
	gera_ementa(LV,E,Total).

/*
Enunciado foi mais ou menos isto:
Calcule o total da fatura. Nota : tenha em conta os descontos patentes na base de conhecimento.
Base de conhecimento:
% desconto(familiaProduto,desconto)
desconto(fruta,0.1).
desconto(detergente,0.15).
desconto(escolar,0.35).
%exemplo de teste
?�geraFatura([(banana,fruta,1.5,2),(pao,padaria,2,1),(caderno,escolar,1,2),Valor).
Valor=6.0.
*/
desconto(fruta,0.1).
desconto(detergente,0.15).
desconto(escolar,0.35).

geraFatura([],0):-!.
geraFatura([(_,Tipo,Preco,Quantidade)|L],Valor):-
	geraFatura(L,Valor1),
	desconto(Tipo,Desc),
	Valor is Valor1 + (Preco * Quantidade * 1 - Desc).
geraFatura([(_,_,Preco,Quantidade)|L],Valor):-
	geraFatura(L,Valor1),
	Valor is Valor1 + (Preco * Quantidade).



/*
geraFatura([],0):-!.
% ITENS COM DESCONTO
geraFatura([(_,FamiliaP,Preco,Quantidade)|L1],Valor):-
geraFatura(L1,Valor1),
desconto(FamiliaP,Desconto),!,
Valor is Valor1+Preco*Quantidade*(1-Desconto).
% ITENS SEM DESCONTO
geraFatura([(_,_,Preco,Quantidade)|L1],Valor):-
geraFatura(L1,Valor1),!,
Valor is Valor1+Preco*Quantidade.
*/
