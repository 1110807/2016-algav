/*
descompactar([],[],[],_).
descompactar([(M,_)|LV],[M|LE],[_|LR],N):-
	descompactar(LV,LE,LR,N).
descompactar([(M,N)|LV],[_|LE],[M|LR],N):-
	N1 is N-1,
	descompactar(LV,LE,LR,N1).

descompactar([],[],[],0).
descompactar([(M,N)|LV],LE,[M|LR],N):-
	\+ member(M,LR),!,
	descompactar(LV,LE,LR,N).

descompactar([(M,N)|LV],[_|LE],[M|LR],N):-
	N1 is N-1,!,
	descompactar(LV,LE,LR,N1).
*/
descompactar([],_,[]).
descompactar([(V,N)|R1], Excl, R2):-
	(member(V,Excl); N = 0),!,
	descompactar(R1, Excl, R2).
descompactar([(V,N)|R1],Excl, [V|R2]):-
	N1 is N - 1,
	descompactar([(V,N1)|R1],Excl,R2).
