/**
Elabore um predicado em Prolog que faz o pr�-procesamento de uma lista de valores, de modo a eliminar e registar os erros ocorridos.
Os erros s�o valores negativos, que ter�o que ser corrgidos para 0. O registo de erros deve ser feito para uma lista que indique qual o erro(valor negativo detetado)
e a sua posi��o na lista.
preProc(ListaValores,ListaCorrigida,ListaErros).
Exemplo:
preProc([1,0,-2,4,-5,200,-2,44,-4],LC,LE).
LC = [1, 0, 0, 4, 0, 200, 0, 44, 0],
LE = [ (-2, 3), (-5, 5), (-2, 7), (-4, 9)].

preProc(LV,LC,LE):-corrigir(LV,LC,LE,0),!,true.


corrigir([],[],[],_).
corrigir([H|LV],[0|LC],[(H,N1)|LE],N):-
	H<0,
	N1 is N+1,
	corrigir(LV,LC,LE,N1).

corrigir([H|LV],[H|LC],LE,N):-
	N1 is N+1,
	corrigir(LV,LC,LE,N1).


preProc(LV,LC,LE):- corrigir(LV,LC,LE,0),!, true.

corrigir([],[],[],_).
corrigir([Valor|LV],[0|LC],[(Valor, C1)|LE],C):-
	Valor < 0,
	C1 is C+1,
	corrigir(LV,LC,LE,C1).
corrigir([Valor|LV],[Valor|LC],LE,C):-
	C1 is C+1,
	corrigir(LV,LC,LE,C1).
*/

proc(N,LV,LC,LE):- eliminar(N,LV,LC,LE,0),!,true.
eliminar(_,[],[],[],_).
eliminar(N,[H|LV],[(H,C1)|LC],LE,C):-
	H < N,
	C1 is C + 1,
	eliminar(N,LV,LC,LE,C1).
eliminar(N,[H|LV],LC,[(H,C1)|LE],C):-
	H > N,
	C1 is C + 1,
	eliminar(N,LV,LC,LE,C1).
