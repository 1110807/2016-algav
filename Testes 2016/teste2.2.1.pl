/*proc(N,C,LV,LI):- inserir(N,C,LV,LI),!,true. */
inserir(_,_,[],[]).
inserir(N,1,LI,[N|LI]).
inserir(N,C,[Z|LV],[Z|LI]):-
	C1 is C - 1,
	inserir(N,C1,LV,LI).

