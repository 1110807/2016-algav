% ----- receitas ------

%produto(Codigo, nome)
produto(1,arroz_doce).
produto(2,acucar).
produto(3,ovos).
produto(4,leite_creme).
produto(5,sumo_limao).
produto(6,arroz).
produto(7,manteiga).
produto(8,scones).
produto(9,leite).
produto(10,baunilha).
produto(11,farinha).



% receita(CodigoReceita, CodigoIngrediente,PesoGramas)
receita(8,7,110).
receita(8,11,450).
receita(8,9,300).
receita(8,10,5).
receita(8,2,50).
receita(4,9,500).
receita(4,11,80).
receita(4,3,60).
receita(1,6,100).
receita(1,2,100).
receita(1,3,80).

ingrediente(C,D):-
	produto(C,D),
	\+receita(C,_,_).

receitasComIngrediente(C):-
	produto(C,N),
	write(C), write(N), nl,
	write('-------'), nl,
	receita(X,C,F),
	write(X),
	produto(X,Z),
	write(Z), write(F), nl,
	fail;true.

/*
ingrediente(C,D):-
	produto(C,D),
	\+receita(C,_,_).



receitasComIngrediente(C):-
	produto(C,N),
	write(C), write(N), nl,
	write('-------'), nl,
	receita(X,C,_),
	write(X), nl,
	produto(C,_),
	fail;true.
*/
