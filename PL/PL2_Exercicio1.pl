potencia(_,0,1):-!.
potencia(X,N,P):-N1 is N-1,
	potencia(X,N1,P1),
	P is X*P1.

/*
pot(_,0,1):-!.
pot((X,Y,R):-
    y<0,
    Y1 is Y-1,
    pot(X,Y1,R1),
    R is X*R1.
*/


factorial(0,1):-!.
factorial(X,N):-X1 is X-1,
	factorial(X1,N1),
	N is X*N1.

divisao(X,Y,0):-X<Y.
divisao(X,Y,R):-X1 is X-Y,
	divisao(X1,Y,R1),
	R is R1+1.

resto(X,Y,X):-X<Y,!.
resto(X,Y,R):-
	X1 is X-Y,
	resto(X1,Y,R).
