cidade(a,45,95).
cidade(b,90,95).
cidade(c,15,85).
cidade(d,40,80).
cidade(e,70,80).
cidade(f,25,65).
cidade(g,65,65).
cidade(h,45,55).
cidade(i,5,50).
cidade(j,80,50).
cidade(l,65,45).
cidade(m,25,40).
cidade(n,55,30).
cidade(o,80,30).
cidade(p,25,15).
cidade(q,80,15).
cidade(r,55,10).

estrada(a,b,45).
estrada(a,c,32).
estrada(a,d,16).
estrada(a,e,30).
estrada(b,e,25).
estrada(d,e,30).
estrada(c,d,26).
estrada(c,f,23).
estrada(c,i,37).
estrada(d,f,22).
estrada(f,h,23).
estrada(f,m,25).
estrada(f,i,25).
estrada(i,m,23).
estrada(e,f,48).
estrada(e,g,16).
estrada(e,j,32).
estrada(g,h,23).
estrada(g,l,20).
estrada(g,j,22).
estrada(h,m,25).
estrada(h,n,27).
estrada(h,l,23).
estrada(j,l,16).
estrada(j,o,20).
estrada(l,n,19).
estrada(l,o,22).
estrada(m,n,32).
estrada(m,p,25).
estrada(n,p,34).
estrada(n,r,20).
estrada(o,n,25).
estrada(o,q,15).
estrada(p,r,31).

caminho(O,D,C,TP):- caminho_la([(0,[O])],D,C,TP).
caminho_la([(TP,[D|R])|_],D,[D|R],TP):-!.
caminho_la([(PA,[X|R1])|R2],D,C,TP):-
	findall((NPA,[Z,X|R1]),(estrada(X,Z,V),not(member(Z,R1)),NPA is PA + V),L1),
		append(L1,R2,R3),
		sort(R3,R4),
		caminho_la(R4,D,C,TP).

/*
caminho(O,D,C,TP):- estimativa(O,D,E), caminho2([(E,0,[O])],D,C,TP).
caminho2([(_,TP,[D|R])|_],D,[D|R],TP):-!.
caminho2([(_,PA,[X|R1])|R2],D,C,TP):-
	findall((E,NPA,[Z,X|R1]),(estrada(X,Z,V),not(member(Z,R1)),estimativa(Z,D,E),NPA is PA + V),L1),
	append(L1,R2,R3), menor(R3,M),
	caminho2(M,D,C,TP).
*/

/*
caminho(O,D,C,TP):- estimativa(O,D,E), caminho2([(E,0,[O])],D,C,TP).
caminho2([(_,TP,[D|R]),D,[D|R],TP):-!.
caminho2([(_,PA,[X|R1]),D,C,TP):-
	findall((E,NPA,[Z,X|R1]),(estrada(X,Z,V),not(member(Z,R1)),estimativa(Z,D,E),NPA is PA + V),L1),
	menor(L,M),
	caminho2(M,D,C,TP).
*/








