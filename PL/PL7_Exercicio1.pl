liga(a,b).
liga(b,a).
liga(a,e).
liga(e,a).
liga(a,d).
liga(d,a).
liga(b,c).
liga(c,b).
liga(b,f).
liga(f,b).
liga(e,f).
liga(f,e).
liga(d,e).
liga(e,d).
liga(d,g).
liga(g,d).
liga(c,f).
liga(f,c).
liga(g,h).
liga(h,g).
liga(f,h).
liga(h,f).
/*
caminhos(O,D,[O|C]):- caminhos(O,D,[O],C).
caminhos(O,O,_,[]).
caminhos(O,D,V,[X|L]):-
	liga(O,X),
	not(member(X,V)),
	caminhos(X,D,[X|V],L).

caminhos(O,C):-caminhos(O,D,C),
	liga(D,O),
	todos_caminhos(L),
	length(L,N),
	length(C,N).

todos_caminhos(L1):-
	findall(X,(liga(X,_);liga(_,X)),L),
	elimina_repetidos(L,L1).



elimina_repetidos([],[]).
elimina_repetidos([X|R1],R2):-
	member(X,R1),!,
	elimina_repetidos(R1,R2).
elimina_repetidos([X|R1],[X|R2]):-
	elimina_repetidos(R1,R2).
*/
caminho(O,D,C):- caminho_la([[O]],D,C).
caminho_la([[D|R]|_],D,[D|R]).
caminho_la([[X|R1]|R2],D,C):-
	findall([Z,X|R1],(liga(X,Z),not(member(Z,R1))),L1),
		append(R2,L1,Lf),
		caminho_la(Lf,D,C).


