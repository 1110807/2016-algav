elimina(_,[],[]).
elimina(X,[X|R],R):-!.
elimina(X,[Y|R1], [Y|R2]):-
	elimina(X,R1,R2).

elimina_todos(_,[],[]).
elimina_todos(X,[X|R1],R2):-!, elimina_todos(X,R1,R2).
elimina_todos(X,[Y|R1], [Y|R2]):-
	elimina_todos(X,R1,R2).

substitui(_,_,[],[]).
substitui(N,X,[N|LV],[X|L]):-
	substitui(N,X,LV,L).
substitui(N,X,[Z|LV],[Z|L]):-
	substitui(N,X,LV,L).

inserir(_,_,[],[]).
inserir(X,1,R,[X|R]).
inserir(X,N,[Y|R1],[Y|R2]):-
	N1 is N - 1,
	inserir(X,N1,R1,R2).

interseccao([],_,[]).
interseccao([X|R1],R2,[X,R3]):-
	membro(X,R2),
	interseccao(R1,R2,R3).
interseccao([_,R1],R2,R3):-
	interseccao(R1,R2,R3).
