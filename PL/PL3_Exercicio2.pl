cor(ceu, azul).
cor(ceu, cinzento).
cor(ceu, preto).
cor(mar, azul).
cor(mar, verde).
cor(mar, cinzento).
cor(via, cinzenta).
cor(via, castanha).
cor(via, preta).
transporte(ceu,aviao).
transporte(ceu,helicoptero).
transporte(ceu,foguete).
transporte(mar,navio).
transporte(mar,lancha).
transporte(via,carro).
transporte(via,camiao).
transporte(via,mota).
transporte(via,autocarro).


nega(X):-
	call(X),!,fail;true.

/*
nega(X):-
	call(X),!,fail.

nega(_).
*/


se_entao(X,Y):- call(X),!,call(Y).
se_entao(_,_).

se_entao_senao(X,Y,Z):-
	call(X),!,call(Y);call(Z).

/*
se_entao_senao(X,Y,Z):-
	call(X),!,call(Y);call(Z).

se_entao_senao(_,_,Z):-
	call(Z).
*/

questao(V):-
	write('Introduza o atributo'),
	read(A),
	write('Introduza o objecto'),
	read(O),
	repeat,
	write('Qual � o valor de'), write(A), write('do'),
	read(V),
	F=..[A,O,V],
	call(F).
