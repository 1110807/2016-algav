liga(a,b).
liga(a,c).
liga(a,d).
liga(b,e).
liga(b,f).
liga(c,f).
liga(c,g).
liga(d,a).
liga(d,h).
liga(d,i).
liga(e,j).
liga(f,a).
liga(f,j).
liga(f,k).
liga(g,f).
liga(g,h).
liga(g,o).
liga(h,d).
liga(h,l).
liga(i,l).
liga(j,m).
liga(j,n).
liga(k,n).
liga(k,p).
liga(l,p).


caminho(O,D,C):-caminho(O,D,[],C).
caminho(O,O,_,[O]).
caminho(O,D,V,[O|L]):-
	liga(O,X),
	\+(member(X,V)),
	caminho(X,D,[X|V],L).

todos_caminhos(O,D,T):-
	findall(C,caminho(O,D,C), T).


menor([X],X).
menor([X,Y|R], M):-
	X < Y,!,
	menor([X|R],M).
menor([_|R],M):-
	menor(R,M).
/*
 * length(L,Nelem).
 *
caminho(X,X,[X]):-write(X).
caminho(X,Y,[Y|L]):-
	liga(Z,Y),
	caminho(X,Z,L).

caminho(X,Z,[X|L]):-
	\+member(L),
	write(L).
*/
