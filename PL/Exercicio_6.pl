homem(joao).
homem(miguel).
homem(antonio).
mulher(maria).
mulher(joana).
mulher(susana).
mulher(ana).
cabelo(joao,loiro).
cabelo(miguel,preto).
cabelo(antonio,preto).
cabelo(joana,loiro).
cabelo(susana,loiro).
cabelo(maria,morena).
cabelo(ana,morena).
rico(miguel).
rico(joana).
gosta_riqueza(joao).
gosta_riqueza(miguel).
gosta_riqueza(joana).
gosta_cabelo(joao,loiro).
gosta_cabelo(miguel,moreno).
possui(ana,carro).
possui(joao,carro).


gosta(X,Y):-mulher(Y),prefere_riqueza(X,Y),prefere_cabelo(X,Y).
/*
gosta(miguel,Y):-mulher(Y),rico(Y),cabelo(Y,moreno).
gosta(joana,Y):-homem(Y),rico(Y),cabelo(Y,preto).
gosta(maria,Y):-cabelo(Y,preto).
*/


prefere_riqueza(X,Y):-gosta_riqueza(X),!,rico(Y).
prefere_riqueza(_,_).
prefere_cabelo(X,Y):-gosta_cabelo(X,C),!,
	cabelo(Y,C).
prefere_cabelo(_,_).

