conta([], 0).
conta([_|T], C):-
	conta(T, C1),
	C is C1 + 1.

soma([],0).
soma([X|T], S):-
	soma(T, S1),
	S is S1+X.

media(L,M):-
	soma(L,S),
	conta(L,C),
	M is S / C.

menor([X], X).
menor([X1,X2|T], M):-
	X1 < X2, !,
	menor([X1|T],M).
menor([_|T],M):-
	menor(T,M).

