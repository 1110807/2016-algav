parent(albert,bob).
parent(albert,betsy).
parent(albert,bill).

parent(alice,bob).
parent(alice,betsy).
parent(alice,bill).

parent(bob,carl).
parent(bob,charlie).

related(X,Y) :-
	parent(X,Y).

related(X,Y) :-
	parent(X,Z),
	related(Z,Y),
	write(Z).

double_digit(X, Y):-
	Y is X*2.

say_hi :-
	write('Como te chamas? '),
	read(X),
	format('Ol� ~w',X).

contar_ate_10(X,X):- write(X),nl.
contar_ate_10(X,Y) :- X<Y,
	write(X), nl,
	X1 is X+1,
	contar_ate_10(X1,Y).

write_list([]).
write_list([Head|Tail]) :-
	write(Head), nl,
	write_list(Tail).
