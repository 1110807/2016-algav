factorial(0,1) :-!.
factorial(X,Y) :-
	X1 is X-1,
	factorial(X1,Y1),
	Y is X*Y1.

divisao(X,Y,0) :- X<Y.
divisao(X,Y,Z) :-
	X1 is X-Y,
	divisao(X1,Y,Z1),
	Z is Z1+1.

liga(a,b).
liga(a,c).
liga(a,d).
liga(b,e).
liga(b,f).
liga(c,f).
liga(c,g).
liga(d,g).
liga(d,h).
liga(d,i).
liga(e,j).
liga(f,j).
liga(f,k).
liga(g,f).
liga(g,o).
liga(g,h).
liga(h,l).
liga(i,l).
liga(j,m).
liga(j,n).
liga(k,n).
liga(k,p).
liga(l,p).

caminho(X,X) :-write(X).
caminho(X,Y) :- liga(Z,Y),
	caminho(X,Z),
	write(Y).

