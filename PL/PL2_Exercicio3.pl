/*
hanoi(N):-move(N,esquerda,centro,direita).
move(0,_,_,_):-!.
move(N,A,B,C):-M is N-1,
move(M,A,C,B),
informa(A,B),
move(M,C,B,A).
informa(A,B):- write('MOVER DISCO DE '),
write(A),write(' PARA '),write(B),nl.
*/
hanoi(N):-
	hanoi(N,esqu,cent,dire,1,_).
hanoi(1,A,B,_,I,I1):-
	write(I), write('Mover o disco de '), write(A), write(' para '), write(B), nl,
	I1 is I + 1.
hanoi(N,A,B,C,I,I3):-
	N1 is N-1,
	hanoi(N1,A,C,B,I,I1),
	hanoi(1,A,B,_,I1,I2),
	hanoi(N1,C,B,A,I2,I3).
