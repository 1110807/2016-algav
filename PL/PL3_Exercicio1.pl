mulher(maria,30).
mulher(rosa,15).
mulher(teresa,21).
homem(predro,18).
homem(tiago,43).

par(X,Y):-
	homem(X,IX),
	mulher(Y,IY).

mostra_par:-
	par(X,Y),
	write(X), write(' '), write(Y), nl,
	fail; true.

escolhe_par(X,Y,MAX,SOM):-
	MAX < 36,
	homem(X,IX),
	mulher(Y,IY),
	SOM is IX+IY,
	SOM =< MAX.
