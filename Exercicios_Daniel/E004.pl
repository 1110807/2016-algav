%P04 (*) Find the number of elements of a list.%
/*
number_list([],_).
number_list(L,N):-
	N is 0,
	number_list2(L,N).
*/
number_list([],0).
number_list([_|L],P):-
	number_list(L,P1),
	P is P1 + 1.
