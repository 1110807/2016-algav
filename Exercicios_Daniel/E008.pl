/*
P08 (**) Eliminate consecutive duplicates of list elements.
If a list contains repeated elements they should be replaced with a single copy of the element. The order of the elements should not be changed.

Example:
?- compress([a,a,a,a,b,c,c,a,a,d,e,e,e,e],X).
X = [a,b,c,a,d,e]

compress([],[]).
compress([X,Y|L1],[X|Lf]):-
	X\=Y,
	compress([L1],[Lf]).
*/

compress([],[]).
compress([X],[X]).
compress([X,X|Xs],Zs) :-
	compress([X|Xs],Zs).

compress([X,Y|Ys],[X|Zs]) :-
	X == Y,
	compress([[Y]|Ys],Zs).
