/*
P15 (**) Duplicate the elements of a list a given number of times.
Example:
?- dupli([a,b,c],3,X).
X = [a,a,a,b,b,b,c,c,c]

What are the results of the goal:
?- dupli(X,3,Y).

dupli(L1,N,L2) :-
	dupli(L1,N,L2,N).

dupli([],_,[],_).
dupli([_|Xs],N,Ys,0) :-
	dupli(Xs,N,Ys,N).
dupli([X|Xs],N,[X|Ys],K) :-
	K > 0,
	K1 is K - 1,
	dupli([X|Xs],N,Ys,K1).
*/

dupli(L1,N,LF):-
	dupli(L1,N,LF,N).

dupli([],_,[],_).
dupli([_|L1],N,LF,0):-
	dupli(L1,N,LF,N).

dupli([X|L1],N,[X|LF],K):-
	K > 0,
	K1 is K - 1,
	dupli([X|L1],N,LF,K1).

