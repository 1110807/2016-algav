% consult('/Users/tiagosantos/Desktop/Universidade/ALGAV/Exames/2017/Recurso.pl').

% Considere uma base de conhecimento de um operador de táxis com factos relativos
% aos transportes realizados por cada táxi e factos com a distância em kms
% entre vários pontos.

% transporte: transp(taxi, nºserviço, pontoOrig, pontoDest).

transp(t1, s1, pt4, pt7).
transp(t2, s1, pt1, pt3).
transp(t1, s2, pt7, pt5).
transp(t3, s1, pt2, pt5).
transp(t2, s2, pt3, pt6).
transp(t3, s2, pt5, pt1).
transp(t1, s3, pt3, pt6).


% Rede de pontos de entrega: liga(pontoOrig, pontoDestino, distancia) (Sentido bidirecional)

liga(pt1, pt2, 21).
liga(pt3, pt4, 55).
liga(pt5, pt3, 30).
liga(pt2, pt5, 19).
liga(pt4, pt2, 13).
liga(pt1, pt6, 28).
liga(pt4, pt7, 12).

% 1. Escreva o predicado servstaxi(T, L) que para um determinado táxi (T) gera uma lista (L)
% com todos os pontos (sem repetições) por onde o táxi circulou. Exemplo:

% ?- servtaxi(t1, L).
% L = [pt5, pt6, pt4, pt7, pt3].

servtaxi(T, L) :-
  findall(Ponto,
  (transp(T, _, Ponto, _);transp(T, _, _, Ponto)
  ), LS), sort(LS, L).


% 2. Escreva o predicado cammin(O, P, C, D) que devolve em C o caminho mínimo e
% a respetiva distância D entre o ponto origem (O) e o ponto destino (P). Exemplo:
% ? - camin(pt1, pt3, C, D).
% C = [pt1, pt2, pt5, pt3],
% D = 70.

cammin(Origem, Destino, Caminho, Distancia) :-
  cammin2(Destino, [(0, [Origem])], Caminho, Distancia).

cammin2(Destino, [(Distancia, [Destino|T])|_], Caminho, Distancia) :-
  reverse([Destino|T], Caminho).

cammin2(Destino, [(Distancia, [H|T])|Resto], Caminho, DistanciaSol) :-
  findall((C, [X,H|T]),
  (H \== Destino,
  (liga(X, H, Dx);liga(H, X, Dx)), \+ member(X, [H|T]), C is Distancia + Dx),
  Novos),
  append(Novos, Resto, Todos),
  sort(Todos, LS),
  cammin2(Destino, LS, Caminho, DistanciaSol).

% 3. Escreva o predicado desempenhotaxi(T, V, D) que para um táxi (T) apresente
% o número de viagens realizadas (V) e a distância total (D) percorrida para
% fazer todas as viagens. Admita que em cada viagem o taxista opta pelo caminho
% com menor distância (utilizar o predicado do exercício anterior).
% Exemplo:
% ?- desempenhotaxi(t1, V, D).
% V = 3,
% D = 154.

% Está a calcular mal o D.

desempenhotaxi(T, V, D) :- findall(Servico, transp(T, Servico, _, _), ListaServicos),
  length(ListaServicos, V),
  findall(Caminho, ((transp(T, _, _, Caminho);transp(T, _, Caminho, _))), ListaCaminhos),
  desempenhoaux(ListaCaminhos, D).

desempenhoaux([X, Y|T], Num) :- cammin(X, Y, _, D), desempenhoaux([Y|T], Num1),
  Num is Num1 + D, !.

desempenhoaux(_, 0).
