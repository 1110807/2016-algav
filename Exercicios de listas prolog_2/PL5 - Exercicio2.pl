/*
Escreva um predicado que permite eliminar todas as ocorrências de um
elemento na lista, conforme exemplo seguinte:
?- elimina_todos(2, [1,2,3,2,4,2],L).
L=[1,3,4].
*/

elimina_todos(_,[],[]).

elimina_todos(N,[X|L1],Lf):-
	N == X,
	elimina_todos(N,L1,Lf).

elimina_todos(N,[X|L1],[X|Lf]):-
	elimina_todos(N,L1,Lf).

