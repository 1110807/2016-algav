/*
Escreva um predicado que permite eliminar apenas a primeira ocorrência
de um elemento na lista, conforme exemplo seguinte:
?- elimina(2,[1,2,3,2,4,2],L).
L=[1,3,2,4,2].
*/

elimina(_,[],[]).

elimina(N,[_|L1],LF):-
	N1 is N - 1,
	N1 == 0,
	elimina(N1,L1,LF).

elimina(N,[X|L1],[X|LF]):-
	N1 is N - 1,
	elimina(N1,L1,LF).
