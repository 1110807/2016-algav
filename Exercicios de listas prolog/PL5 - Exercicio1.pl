/*
Escreva um predicado que permite eliminar apenas a primeira ocorrência
de um elemento na lista, conforme exemplo seguinte:
?- elimina(2,[1,2,3,2,4,2],L).
L=[1,3,2,4,2].
*/

elimina(_,[],[]).

elimina(X,[X|L1],L1):-!.

elimina(X,[Y|L1],[Y|LF]):-
	elimina(X,L1,LF).
