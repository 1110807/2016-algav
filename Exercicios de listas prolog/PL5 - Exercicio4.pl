/*
Escreva um predicado que permite substituir todas as ocorrências de um
dado elemento numa lista por outro elemento, conforme exemplo
seguinte:
?- substitui(2,6,[1,2,3,2,4,2],L).
L=[1,6,3,6,4,6]

substitui(_,_,[],[]).

substitui(P,V,[_|L1],[V|LF]):-
	P1 is P - 1,
	P1 == 0,
	substitui(P1,V,L1,LF).

substitui(P,V,[X|L1],[X|LF]):-
	P1 is P - 1,
	substitui(P1,V,L1,LF).
*/
substitui(_,_,[],[]).

substitui(P,V,[P|L1],[V|LF]):-
	substitui(P,V,L1,LF).

substitui(P,V,[X|L1],[X|LF]):-
	substitui(P,V,L1,LF),!.
