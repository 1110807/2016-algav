/*
Escreva um predicado para inserir um elemento numa dada posi��o
numa lista, conforme exemplo seguinte:
?- insere(7,4,[1,2,3,2,4,2],L).
L=[1,2,3,7,2,4,2]
*/

insere(_,_,[],[]).

insere(V,P,L1,[V|LF]):-
	P1 is P-1,
	P == 0,
	insere(V,P1,L1,LF).

insere(V,P,[X|L1],[X|LF]):-
	P1 is P - 1,
	insere(V,P1,L1,LF).
