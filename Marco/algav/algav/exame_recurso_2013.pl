%utilizador(nickname,email,local_actual,lista_amigos)
utilizador(nick0,email0,vilareal,[nick1,nick8]).
utilizador(nick1,email1,lisboa,[nick0,nick3,nick5,nick9]).
utilizador(nick2,email2,porto,[nick5,nick7,nick8]).
utilizador(nick3,email3,braga,[nick1,nick4]).
utilizador(nick4,email4,coimbra,[nick1,nick3,nick2,nick5,nick6]).
utilizador(nick5,email5,porto,[nick1,nick4,nick2]).
utilizador(nick6,email6,lisboa,[nick3,nick2]).
utilizador(nick7,email7,braga,[nick3,nick5]).
utilizador(nick8,email8,porto,[nick3,nick5,nick7]).
utilizador(nick9,email9,vilareal,[nick1,nick4]).

%local(cidadeA,cidadeB,dist)
local(lisboa,coimbra,210).
local(braga,porto,55).
local(vilareal,braga,230).
local(coimbra,vilareal,188).
local(porto,coimbra,130).
local(lisboa,faro,280).

locais(NU,LC):-
	findall((Local,Numero),((local(Local,_,_);local(_,Local,_)),locaisAux(Local,L),length(L,Numero),Numero>=NU),LL),
	sort(LL,LC).


locaisAux(Local,L):-findall((Local),(utilizador(_,_,Local,_)),L).


amigospocidade(U,LCA):-
	utilizador(U,_,_,LA),
	findall((Local,Amigos),(bagof(Amigo,(utilizador(Amigo,_,Local,_),member(Amigo,LA)),Amigos)),LCA).
