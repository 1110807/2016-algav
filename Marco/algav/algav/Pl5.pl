/* ---- 1 ---- */
%a


elimina(Num,[Num|T],T).
elimina(Num,[H|T],[H|T2]):-
	elimina(Num,T,T2).

%b

elimina_todos(_,[],[]).

elimina_todos(Num,[Num|T],T2):-
	elimina_todos(Num,T,T2).
elimina_todos(Num,[H|T],[H|T2]):-
	      elimina_todos(Num,T,T2).



/* ---- 3 --- */

substitui(_,_,[],[]).

substitui(Num,NNum,[Num|T],[NNum|T2]):-
	substitui(Num,NNum,T,T2).

substitui(Num,NNum,[H|T],[H|T2]):-
	substitui(Num,NNum,T,T2).


/* --- 4 --- */
insere(Num,Pos,L,NL):-
	insere(Num,Pos,1,L,NL).

insere(Num,Pos,Pos,[H|T],[Num,H|T]).

insere(Num,Pos,Cont,[H|T],[H|T2]):-
	Cont1 is Cont +1,
	insere(Num,Pos,Cont1,T,T2).

/* --- 5 --- */

inverte(L,LI):- inverte1(L,[ ],LI).
inverte1([ ],L,L).
inverte1([X|L],LI,LF):- inverte1(L,[X|LI],LF).

/* --- 6 --- */

uni�o([ ],L,L).
uni�o([X|L],L1,L2):- member(X,L1),!,uni�o(L,L1,L2).
uni�o([X|L],L1,[X|L2]):- uni�o(L,L1,L2).

/* --- 7 --- */

totobola([],[]).
totobola([H|T],[X|T2]):-
	is_list(H),!,
	member(X,H),
	totobola(T,T2).

totobola([H|T],[H|T2]):-
	totobola(T,T2).


/* --- 8 --- */
totoloto(L,LR):-
	totoloto(L,LR,0).

totoloto(_,[],6).
totoloto([H|T],[H|T2],Cont):-
	Cont1 is Cont + 1,
	totoloto(T,T2,Cont1).

totoloto([_|T],T2,C):-
	totoloto(T,T2,C).


/* --- 9 --- */

datas_jogo([],[],LR):- reverse(LR,LR).
datas_jogo([H|T],[H2|T2],L):-
	datas(H,H2,LR),
	append(LR,L,NL),
	datas_jogo(T,T2,NL).



datas(EquipaA,EquipaB,NL):-

	assertz(EquipaA),
	t(X,Y),
	write(X),nl,write(Y),nl,
	retract(EquipaA),

	assertz(EquipaB),
	t(X2,Y2),
	write(X2),nl,write(Y2),nl,
	retract(EquipaB),
	real_Data(X,Y,X2,Y2,NL).

real_Data(DiA,DfA,DiB,DfB,NL):-
       maiorDiaInicial(DiA,DiB,DiR),
       menorDiaFinal(DfA,DfB,DfR),
       NL = [t(DiR,DfR)].



maiorDiaInicial(DiA,DiB,DiA):- DiA > DiB,!.
maiorDiaInicial(_,DiB,DiB).

menorDiaFinal(DfA,DfB,DfA):- DfA < DfB,!.
menorDiaFinal(_,DfB,DfB).

/*

	assertz(H),
	t(X,Y),
	write(X),nl,write(Y),nl,
	retract(H),
	assertz(H2),
	t(X2,Y2),
	write(X2),nl,write(Y2),nl,
	retract(H2).

*/
