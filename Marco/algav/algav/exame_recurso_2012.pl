%cidade(Nome_da_cidade,TemperatiraMedia)
cidade(lisboa,20).
cidade(berlin,10).
cidade(madrid,22).
cidade(londres,7).
cidade(madeira,22).

% voo(CidadeA,CidadeB,Numero_de_horas_do_voo,numero_voo,companhia_aviacao)
voo(lisboa,londres,2,123,tap).
voo(lisboa,madeira,1.5,124,tap).
voo(berlin,londres,2,323,lufthansa).
voo(berlin,madrid,1.5,421,easyjet).
voo(lisboa,madrid,1,512,iberia).

companhia(tap,regular).
companhia(iberia,regular).
companhia(lufthansa,regular).
companhia(easyjet,'low cost').


%1

cidades(CA,T,LC):-
	findall(C,
		((voo(C,_,_,_,CA);(voo(_,C,_,_,CA))),cidade(C,Temp),Temp>T)
	       ,LCT),
	sort(LCT,LC).

%2

procura_voos(O,D,T,L):-hbf(O,D,T1,L),converteT(T1,T).

converteT([],[]).
converteT([H,H1|T],[N|L]):-
	converteT(T,L),
	(voo(H,H1,_,N,_);voo(H1,H,_,N,_)),!.

hbf(Orig,Dest,Perc,Total):-
	estimativa(Orig,Dest,H), F is H + 0, % G = 0
	hbf1([c(F/0,[Orig])],Dest,P,Total),!,
	reverse(P,Perc).

hbf1(Percursos,Dest,Percurso,Total):-
	menor_percursoh(Percursos,Menor,Restantes),
	percursos_seguintesh(Menor,Dest,Restantes,Percurso,Total).

percursos_seguintesh(c(_/Dist,Percurso),Dest,_,Percurso,Dist):- Percurso=[Dest|_].
percursos_seguintesh(c(_,[Dest|_]),Dest,Restantes,Percurso,Total):-!,
	hbf1(Restantes,Dest,Percurso,Total).
percursos_seguintesh(c(_/Dist,[Ult|T]),Dest,Percursos,Percurso,Total):-
	findall(c(F1/D1,[Z,Ult|T]),proximo_noh(Ult,T,Z,Dist,Dest,F1/D1),Lista),
	append(Lista,Percursos,NovosPercursos),
	hbf1(NovosPercursos,Dest,Percurso,Total).
menor_percursoh([Percurso|Percursos],Menor,[Percurso|Resto]):-
	menor_percursoh(Percursos,Menor,Resto),
	menorh(Menor,Percurso),
	!.

menor_percursoh([Percurso|Resto],Percurso,Resto).

menorh(c(H1/D1,_),c(H2/D2,_)):-C1 is H1+D1,C2 is H2+D2, C1<C2.


proximo_noh(X,T,Y,Dist,Dest,F/Dist1):-
	(voo(X,Y,Z,_,_);voo(Y,X,Z,_,_)),
	\+ member(Y,T),
	Dist1 is Dist + Z,
	estimativa(Y,Dest,H), F is H + Dist1.

%estimativa(C1,C2,Est):-
%	cidade(C1,X1,Y1),
%	cidade(C2,X2,Y2),
%	DX is X1-X2,
%	DY is Y1-Y2,
%	Est is sqrt(DX*DX+DY*DY).

estimativa(_,_,0). % para desprezar a heurística.
