dstposto(pt1,pt2, 21).
dstposto(pt3,pt4, 55).
dstposto(pt5,pt3, 30).
dstposto(pt2,pt5, 19).
dstposto(pt4,pt2, 13).
dstposto(pt1,pt6, 28).

abastrobot(r1,[pt4,pt5,pt2,pt6,pt5]).
abastrobot(r2,[pt1,pt3,pt6]).
abastrobot(r3,[pt2,pt5,pt3,pt2,pt5,pt6,pt1,pt5]).


%%      Exercicio 1
abastposto(P,L):-
	findall((Robot,Vezes),(abastrobot(Robot,A),contaElementos(P,A,Vezes)),L).

contaElementos(_,[],0).

contaElementos(P,[P|T],N):-
	contaElementos(P,T,N1),!,
	N is N1+1.

contaElementos(P,[H|T],N):-
	P\=H,
	contaElementos(P,T,N).

%%	Exercicio 2

hbf(Orig,Dest,Perc,Total):-
	estimativa(Orig,Dest,H), F is H + 0, % G = 0
	hbf1([c(F/0,[Orig])],Dest,P,Total),!,
	reverse(P,Perc).

hbf1(Percursos,Dest,Percurso,Total):-
	menor_percursoh(Percursos,Menor,Restantes),
	percursos_seguintesh(Menor,Dest,Restantes,Percurso,Total).

percursos_seguintesh(c(_/Dist,Percurso),Dest,_,Percurso,Dist):- Percurso=[Dest|_].
percursos_seguintesh(c(_,[Dest|_]),Dest,Restantes,Percurso,Total):-!,
	hbf1(Restantes,Dest,Percurso,Total).
percursos_seguintesh(c(_/Dist,[Ult|T]),Dest,Percursos,Percurso,Total):-
	findall(c(F1/D1,[Z,Ult|T]),proximo_noh(Ult,T,Z,Dist,Dest,F1/D1),Lista),
	append(Lista,Percursos,NovosPercursos),
	hbf1(NovosPercursos,Dest,Percurso,Total).
menor_percursoh([Percurso|Percursos],Menor,[Percurso|Resto]):-
	menor_percursoh(Percursos,Menor,Resto),
	menorh(Menor,Percurso),
	!.

menor_percursoh([Percurso|Resto],Percurso,Resto).

menorh(c(H1/D1,_),c(H2/D2,_)):-C1 is H1+D1,C2 is H2+D2, C1<C2.


proximo_noh(X,T,Y,Dist,Dest,F/Dist1):-
	(dstposto(X,Y,Z);dstposto(Y,X,Z)),
	\+ member(Y,T),
	Dist1 is Dist + Z,
	estimativa(Y,Dest,H), F is H + Dist1.

%estimativa(C1,C2,Est):-
%	cidade(C1,X1,Y1),
%	cidade(C2,X2,Y2),
%	DX is X1-X2,
%	DY is Y1-Y2,
%	Est is sqrt(DX*DX+DY*DY).

estimativa(_,_,0). % para desprezar a heurística.


%%      exercicio 3

desempenhorobot(R,P,D):-
	abastrobot(R,A),
	percorrelista(A,_,D),!,
        contaUnicos(A,P,[]),!.

percorrelista([_|[]],[],0).

percorrelista([H,H2|T],P,D):-
	hbf(H,H2,PartialP,ParcialD),
	concatAux(P1,PartialP,P),
	percorrelista([H2|T],P1,D1),
	D is D1 + ParcialD.


concatAux([],X,X).
concatAux([H|T],L,[H|T1]):-concatAux(T,L,T1).

contaUnicos([],0,_).
contaUnicos([H|T],N,Aux):-
	\+ member(H,Aux),!,
	contaUnicos(T,N1,[H|Aux]),
	N is N1+1.

contaUnicos([_|T],N,Aux):-
	contaUnicos(T,N,Aux).
