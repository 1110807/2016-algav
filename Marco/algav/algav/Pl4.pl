membro(X,[X|_]).
membro(X,[_|L]):-membro(X,L).

concatena([ ],L,L).
concatena([A|B],C,[A|D]):-concatena(B,C,D).


%3

/* ----- Conta Elementos da Lista ---- */

conta([],0).
conta([_|T],R):-
	conta(T,R1), R is R1+1.

/* ----- Soma Elementos da Lista ----- */

soma([],0).
soma([H|T],R):-
	soma(T,R1), R is R1+H.


/* ---- Media dos elementos da Lisa ---- */

media([],_):-!, fail.
media(L,R):-
	conta(L,Rc), soma(L,Rs),
	R is Rs / Rc .

/* ----- Menos numero de uma Lista --- */

menor([],9999).
menor([H|T], R):-
	menor(T,R1), (H < R1, R = H; H).
