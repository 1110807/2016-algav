utilizador(pedro,estudante).
utilizador(joao,designer).
utilizador(joaquim,informatico).
app('angry birds',jogo).
app('fruit ninja',jogo).
app(ebuddy,social).
app('Adobe Reader',produtividade).
amigo(pedro,joao).
amigo(joaquim,joao).
recomenda(pedro,'Adobe Reader').
recomenda(joao,ebuddy).
recomenda(joao,'Adobe Reader').
instalado(pedro,'angry birds').
instalado(joao,ebuddy).
instalado(joaquim, 'fruit ninja').
instalado(joao,'angry birds').

recomendados(User,Cat,AR):-
	findall(App,(recomenda(User,App),app(App,Cat)),AR).

ligado(A,B,C):-ligado(A,B,[A],C),!.
ligado(B,B,_,[B]).
ligado(A,B,CA,[A|C]):-(amigo(A,X);amigo(X,A)),\+member(X,CA),ligado(X,B,[X|CA],C).

sugestoes(User,LA):-findall(App,(app(App,_),\+instalado(User,App),recomenda(User,A),recomenda(U,A),recomenda(U,App),U\=User),LA).
