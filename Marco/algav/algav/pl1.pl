homem(anibal).
homem(manuel).
homem(jose).
homem(fernando).
homem(francisco).
homem(alvaro).
homem(paulo).
homem(carlos).
homem(ricardo).
homem(daniel).
homem(samuel).

mulher(maria).
mulher(alzira).
mulher(marta).
mulher(diana).
mulher(celia).
mulher(sara).
mulher(fatima).
mulher(zulmira).
mulher(delfina).
mulher(adriana).

casados(manuel,maria).
casados(anibal,alzira).
casados(jose,marta).
casados(fernando,diana).
casados(alvaro,celia).
casados(carlos,zulmira).
casados(paulo,fatima).
casados(maria,manuel).
casados(alzira,manuel).
casados(marta,jose).
casados(diana,fernando).
casados(celia,alvaro).
casados(zulmira,carlos).
casados(fatima,paulo).

descendente_directo(marta,manuel,maria).
descendente_directo(fernando,manuel,maria).
descendente_directo(diana,anibal,alzira).
descendente_directo(francisco,anibal,alzira).
descendente_directo(alvaro,anibal,alzira).
descendente_directo(delfina,jose,marta).
descendente_directo(carlos,fernando,diana).
descendente_directo(paulo,fernando,diana).
descendente_directo(sara,alvaro,celia).
descendente_directo(ricardo,carlos,zulmira).
descendente_directo(daniel,carlos,zulmira).
descendente_directo(adriana,paulo,fatima).
descendente_directo(samuel,paulo,fatima).

filho(X,Y):-homem(X),(descendente_directo(X,Y,_);descendente_directo(X,_,Y)).
filha(X,Y):-mulher(X),(descendente_directo(X,Y,_);descendente_directo(X,_,Y)).

pai(X,Y):-homem(X),(filho(Y,X);filha(Y,X)).
mae(X,Y):-mulher(X),(filho(Y,X);filha(Y,X)).

avoM(X,Y):-pai(X,Z), (pai(Z,Y);mae(Z,Y)).
avoF(X,Y):-mae(X,Z), (pai(Z,Y);mae(Z,Y)).

neto(X,Y):-homem(X), (avoM(Y,X);avoF(Y,X)).
neta(X,Y):-mulher(X), (avoM(Y,X);avoF(Y,X)).

irmao(X,Y):-homem(X),descendente_directo(X,P,M),descendente_directo(Y,P,M),X\=Y.
irma(X,Y):-mulher(X),descendente_directo(X,P,M),descendente_directo(Y,P,M),X\=Y.

tio(X,Y):- homem(X), casados(X,C),
	(irmao(X,Z),(pai(Z,Y);mae(Z,Y)));
	 (irma(C,Z),(pai(Z,Y);mae(Z,Y))).


tia(X,Y):- mulher(X), casados(X,C),
	(irma(X,Z),(pai(Z,Y);mae(Z,Y)));
	 (irmao(C,Z),(pai(Z,Y);mae(Z,Y))).

sobrinho(X,Y):- homem(X), (tio(Y,X);tia(Y,X)).
sobrinha(X,Y):- mulher(X), (tia(Y,X);tio(Y,X)).

primo(X,Y):-homem(X),(descendente_directo(X,Y,_);descendente_directo(X,_,Y)).
prima(X,Y):-homem(X),(descendente_directo(X,Y,_);descendente_directo(X,_,Y)).

cunhado(X,Y):-homem(X),(casados(X,Z),(irmao(Z,Y);irma(Z,Y)));
	(casados(Z,X),(irmao(Z,A);irma(Z,A)),casados(A,Y));
	(irmao(X,L);irma(L,X),casados(L,Y)).

cunhada(X,Y):-mulher(X),(casados(X,Z),(irmao(Z,Y);irma(Z,Y)));
	(casados(Z,X),(irmao(Z,A);irma(Z,A)),casados(A,Y));
	(irmao(X,L);irma(L,X),casados(L,Y)).


descendentes(X):- (descendente_directo(Y,_,X);descendente_directo(Y,X,_)), imprime(Y),descendentes(Y).


imprime(X):- write(X),nl.


ascendentes(X):- pai(Y,X), imprime(Y),
	mae(Z,X), imprime(Z),
	((ascendentes(Z),ascendentes(Y));
	ascendentes(Y),ascendentes(Z)).
