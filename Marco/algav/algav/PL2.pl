/* 1 */

/* A */

potencia(_,0,1).
potencia(B,E,R):-E > 0, E1 is E-1, potencia(B,E1,R1), R is B * R1.

/* B */

factorial(X):- Y is X - 1,factorial(X,Y).
factorial(X,1):- write(X).
factorial(X,Y):- Z is X * Y, W is Y - 1,
	factorial(Z,W).

/* C */

divisao(Dividendo,Divisor,0):- Dividendo < Divisor.
divisao(Dividendo,Divisor,Quociente):- Dividendo >= Divisor,
	NovoDiv is Dividendo - Divisor,
	divisao(NovoDiv,Divisor,Q1), Quociente is Q1+1.

/* D */

restoDivisao( Dividendo, Divisor, Dividendo) :- Dividendo < Divisor.
restoDivisao(Dividendo,Divisor,Resto) :- Dividendo >= Divisor,
	NovoDiv is Dividendo - Divisor,
	restoDivisao(NovoDiv,Divisor,Resto).

/*FIM 1 */




/* 2 */

/* BASE DE CONHECIMENTO 2 */
liga(a,b).
liga(a,c).
liga(a,d).

liga(b,e).
liga(b,f).

liga(c,f).
liga(c,g).

liga(d,g).
liga(d,h).
liga(d,i).

liga(e,j).

liga(f,j).
liga(f,k).

liga(g,f).
liga(g,o).
liga(g,h).

liga(h,l).

liga(i,l).

liga(j,m).
liga(j,n).

liga(k,n).
liga(k,p).

liga(l,p).

/* PREDICADOS 2 */

caminhos(O,O).
caminhos(O,D):- liga(O,D).
caminhos(O,D):- liga(O,Y), write(Y),nl,
	caminhos(O,D,Y).
caminhos(_,D,Y):- liga(Y,D).
caminhos(_,D,Y):- caminhos(Y,D).


/* 3 */

/* A */

/*
hanoi(N):-move(N,esquerda,centro,direita).
move(0,_,_,_):-!.
move(N,A,B,C,Num,_):-M is N-1,
	move(M,A,C,B),
	informa(A,B),
move(M,C,B,A).
informa(A,B,Num1):- write('MOVER DISCO DE '),
	write(A),write(' PARA '),write(B),nl.

*/

/* B */
hanoi(N):-move(N,esquerda,centro,direita,1).
move(0,_,_,_,_):-!.
move(N,A,B,C,Num):-M is N-1,
	move(M,A,C,B,_),
	Num1 is Num+1,
	informa(A,B,Num),
move(M,C,B,A,Num1).
informa(A,B,Num):- write(Num),write(':MOVER DISCO DE '),
	write(A),write(' PARA '),write(B),nl.







