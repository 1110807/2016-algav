%produto (nome, produto, pre�o)
produto(p1,423,11).
produto(p2,567,197).
produto(p3,128,99).
produto(p4,256,299).
produto(p5,511,347).

%stock (produto, quantidade)
stock(423,36).
stock(567,200).
stock(128,5).
stock(256,93).
stock(511,489).

%encomenda(cliente, produto, quantidade)
encomenda('M. Santos',423,30).
encomenda('E. Silva',567,100).
encomenda('B. Costa',511,194).
encomenda('B. Costa',256,972).
encomenda('M. Santos',128,12).

/*
1 - Escreva um predicado que lista todas as encomendas de um dado cliente apresentando em cabe�alho o nome do cliente e linha a linha os produtos encomendados: <cod-prod>,<produto>,<qnt-enc>,<valor-encomendado>.

?- lista('M. Santos').
Cliente: M. Santos
------------------
423, p1, 30, 330
128, p3, 12, 1188
true.

2 - Escreva o predicado prontoaExpedir que para a base de conecimento acima apresenta os clients cujas encomendas podem ser satisfeitas na totalidade.

?- prontaExpedir(C).
C= 'E.Silva'
false.
*/

lista(N):-
	write('Cliente: '), write(N), nl,
	encomenda(N,X,Y),
	produto(P,X,Z),
	S is Y * Z,
	write(X), write(P), write(Y), write(S),
	nl,
	fail;true.


prontaExpedir(C):-
	encomenda(C,_,_),
	\+ (encomenda(C,P,Q),stock(P,Q1), Q >= Q1),
	write("C = "), write(C),
	nl,fail.
/*
comStock(C):-
	encomenda(C,P,Q),
	stock(P,X),
	X > Q.

semStock(C):-
	encomenda(C,P,Q),
	stock(P,X),
	X < Q.

prontaExpedir(C):- write('C = '), write(C), nl,
	encomenda(C,_,_),
	comStock(C),
	prontaExpedir(C),
	fail.

*/
