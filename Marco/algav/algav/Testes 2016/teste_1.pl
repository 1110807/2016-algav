cargo(tecnico, ivone).
cargo(engenheiro, daniel).
cargo(engenheiro, isabel).
cargo(engenheiro, oscar).
cargo(engenheiro, tomas).
cargo(engenheiro, ana).
cargo(analista, rui).
cargo(supervisor, luis).
cargo(supervisor_chefe, sonia).
cargo(secretaria_exec, laura).
cargo(diretor, santiago).

chefiado_por(tecnico, engenheiro).
chefiado_por(engenheiro, supervisor).
chefiado_por(analista, supervisor).
chefiado_por(supervisor, supervisor_chefe).
chefiado_por(supervisor_chefe, diretor).
chefiado_por(secretaria_exec, diretor).

/*
Perguntas:
1) Defina uma regra cargoSemChefia(C) que permita listar as posi��es sem chefia (que n�o podem ser chefes).
Resultados esperados:
?- cargoSemChefia(C).
- Tecnico
- Analista
- Secretaria_exec
2) Defina uma regra nivelHierarq(P, N) que tenha como resultado o n�vel hier�rquico de uma dada posi��o.
Resultados Esperados:
?- nivelHierarq(T�cnico, N).
- N = 0
?- nivelHierarq(Director, N).
- N = 4
*/

cargoSemChefia(C):-
	cargo(C,_),
	\+chefiado_por(_,C).

nivelHierarq(tecnico,0).
nivelHierarq(analista,0).
nivelHierarq(secretaria_exec,0).

nivelHierarq(P,N):-
	chefiado_por(Y,P),
	nivelHierarq(Y,N1),
	N is N1 + 1.
