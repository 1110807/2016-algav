%
continente(asia).
continente(america).
continente(europa).
continente(africa).
continente(oceania).
pais(portugal, europa).
pais(espanha, europa).
pais(franca, europa).
pais(belgica, europa).
pais(austria, europa).
pais(alemanha, europa).
pais(polonia, europa).
pais(holanda, europa).
pais(eua, america).
pais(brasil, america).
pais(china, asia).
pais(mongolia, asia).
fronteira(portugal, espanha).
fronteira(franca, espanha).
fronteira(franca,belgica).
fronteira(belgica, alemanha).
fronteira(belgica, holanda).
fronteira(alemanha, polonia).
fronteira(alemanha, austria).
fronteira(china, mongolia).
/*
% alinea 1
% % encontar continentes sem paises representados

%2 Listar paises que � facil l� chegar (pais que fa�a fronteira ou que tem apenas um pa�s pelo meio: Ex. Franca: Espanha, Portugal, Alemanha, etc.)
facil_La_Chegar(C):- ??
*/

contSemPaises(C):-
	continente(C),
	\+pais(_,C).

vizinho(P1,P2):- fronteira(P1,P2); fronteira(P2,P1).

facil_la_chegar(C,P):-
	vizinho(C,P),
	write(P), nl,
	vizinho(P,Z),
	write(Z), nl,
	fail;true.


% alinea 2
%
/*
facil_la_chegar(C):-
	fronteira(C,X),
	write(X), nl,
	facil_la_chegar(X).
*/
%vizinho(P1,P2):-fronteira(P1,P2);fronteira(P2,P1).
/*

chego_la_facil(P1,P2):-vizinho(P1,P2);
(vizinho(P1,P2),P1\=P2).

chego_la_facil2(P,P2):-fronteira(P,P1),
fronteira(P1,P2);
fronteira(P,P2);fronteira(P2,_),!.


facil_la_cegar(C):-
	vizinho(C,P


facil_la_chegar(C,P1):-
	C\=P1;
	fronteira(C,P1);fronteira(P1,C),
	write(P1), nl,
	facil_la_chegar(P1,X).

facil_la_chegar(C):-(vizinho(C,Z);vizinho(P1,Z)),P1\=C.	fail;true.

*/
