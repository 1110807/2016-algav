homem(manuel).
mulher(maria).
homem(anibal).
mulher(alzira).
homem(jose).
mulher(marta).
homem(fernando).
mulher(diana).
homem(francisco).
mulher(celia).
homem(alvaro).
mulher(delfina).
mulher(zulmira).
homem(carlos).
mulher(fatima).
homem(paulo).
homem(ricardo).
homem(daniel).
homem(samuel).
mulher(sara).
mulher(adriana).
casados(manuel,maria).
casados(anibal,alzira).
casados(jose,marta).
casados(fernando,diana).
casados(alvaro,celia).
casados(carlos,zulmira).
casados(paulo,fatima).
descendente_directo(marta,manuel,maria).
descendente_directo(fernando,manuel,maria).
descendente_directo(diana,anibal,alzira).
descendente_directo(francisco,anibal,alzira).
descendente_directo(alvaro,anibal,alzira).
descendente_directo(delfina,jose,marta).
descendente_directo(carlos,fernando,diana).
descendente_directo(zulmira,fernando,diana).
descendente_directo(paulo,fernando,diana).
descendente_directo(sara,alvaro,celia).
descendente_directo(ricardo,carlos,zulmira).
descendente_directo(daniel,zulmira,carlos).
descendente_directo(adriana,paulo,fatima).
descendente_directo(samuel,paulo,fatima).
filho(X,Y):-homem(X),descendente_directo(X,Y,_).
filho(X,Y):-homem(X),descendente_directo(X,_,Y).
filha(X,Y):-mulher(X),descendente_directo(X,Y,_).
filha(X,Y):-mulher(X),descendente_directo(X,_,Y).
pai(X,Y):- homem(X),descendente_directo(Y,X,_).
pai(X,Y):- mulher(X),descendente_directo(Y,X,_).
mae(X,Y):- homem(X),descendente_directo(Y,_,X).
mae(X,Y):- mulher(X),descendente_directo(Y,_,X).
avu(X,Y):- pai(X,Z), (pai(Z,Y);mae(Z,Y)).
avo(X,Y):- mae(X,Z), (pai(Z,Y);mae(Z,Y)).
neto(X,Y):- homem(X),filho(X,Z), (filho(Z,Y)).
neta(X,Y):- mulher(X),filho(X,Z), (filho(Z,Y)).
irmao(X,Y):- homem(X),(filho(X,Y);filha(X,Y)).












