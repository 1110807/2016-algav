linha(1,[a,b,c,d,e]).
linha(2,[b,g,f,e,i,h]).
linha(3,[l,m,g,n,f,o,p,q,i,j]).


mesma_linha(A,B):-
	findall(Ln,(linha(Ln,X),member(A,X),member(B,X)),L),
	length(L,Ln),
	Ln>0.

muda_linha(E,L,NL):-
	findall(NewL,(linha(NewL,LE),\+member(NewL,L),member(E,LE)),NL),
	length(NL,Ln),
	Ln>0.


liga(A,B):-(linha(_,X),member(A,X),liga2(X,A,B)).

liga2([],_,_):-fail.
liga2([_|T],A,B):-liga2(T,A,B).
liga2([H,B|_],H,B).
liga2([B,H|_],H,B).
liga2([H|T],H,B):-last(T,B).


caminho(O,D,L):- caminho(O,D,[O],L).
caminho(D,D,_,[D]).
caminho(O,D,LA,[O|L]):- liga(O,X), \+member(X,LA),
	caminho(X,D,[X|LA],L).

todos_caminhos(O,D,L):- findall(P,caminho(O,D,P),L).
