programador(pedro, 18).
programador(jo�o, 30).
programador(joaquim, 22).
programador(antonio, 45).

equipa(alfa, pedro).
equipa(beta, jo�o).
equipa(beta, joaquim).
equipa(beta, antonio).

programa(pedro,'c#').
programa(jo�o,'.net c').
programa(jo�o,'c cardinal').

mesmaLinguagem('c#', 'c sharp').
mesmaLinguagem('c#', 'c cardinal').
mesmaLinguagem('c sharp', '.net c').
mesmaLinguagem('c cardinal', 'visualstudio c').
mesmaLinguagem('.net c', 'c#').

programadorGrupo(P,G):-equipa(G,P).

elementosDeUmaEquipa(Equipa,N):-
	findall(Pessoa,(equipa(Equipa,Pessoa)),N).

equipa_idade_minima(NomeEquipa,LimIdade,Lee):-
	findall(Pessoa,(equipa(NomeEquipa,Pessoa),programador(Pessoa,Idade),Idade>=LimIdade),Lee),
	elementosDeUmaEquipa(NomeEquipa,L),
	length(Lee,X),length(L,X1),
	X==X1.

equivalentes(Ling,OutraLing,Liga):-
	findall((Ling,NomeL,OutraLing),(mesmaLinguagem(Ling,NomeL),mesmaLinguagem(NomeL,OutraLing)),Liga).

