/* --- Base De Conhecimento --- */

liga(a,b).
liga(a,c).
liga(a,d).
liga(b,e).
liga(b,f).
liga(c,f).
liga(c,g).
liga(d,a).
liga(d,g).
liga(d,h).
liga(d,i).
liga(e,j).
liga(f,j).
liga(f,k).
liga(g,f).
liga(g,o).
liga(g,h).
liga(h,l).
liga(h,d).
liga(i,l).
liga(j,m).
liga(j,n).
liga(k,n).
liga(k,p).
liga(l,p).


/* --- 1 --- */

/* --- A --- */

caminho(O,O):- write(O).
caminho(O,D):-
	liga(O,D),
	write(D).
caminho(O,D):-
	liga(O,X),nl,
	write(X),
	caminho(X,D).



/* --- Inserir Caminho Numa Lista --- */

%Pesquisa Primeiro em profundidade
listaCaminhos(O,D,C):- listaCaminhos(O,D,[O],C).
listaCaminhos(D,D,_,[D]).
listaCaminhos(O,D,L,[O|R]):-liga(O,D1), \+ member(D1,L),  listaCaminhos(D1,D,[D1|L],R).

/* --- B --- */

todosCaminhos(O,D,L):- findall(LF,listaCaminhos(O,D,LF),L).

/* --- C --- */

menorCaminho(O,D,L):- todosCaminhos(O,D,LT), menor(LT,L,[]).
menor([],_,_).
menor([H|T],H,M):- M == [], menor(T,H,H).
menor([H|T],H,M):- length(H,Tam1), length(M,Tam2),
	Tam1<Tam2, menor(T,H,H).
menor([_|T],L,M):-
	menor(T,L,M).
