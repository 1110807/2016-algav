%componente(codigo,nome,tipo,marca,performance,pre�o).
componente(1, 'MAXIMUS VI GENE',motherboard, asus,9, 100).
componente(2, 'DH55TC',motherboard, intel, 5, 60).
componente(3, 'Intel740',grafica, intel, 8, 30).
componente(4, 'Intel752',grafica, intel, 4, 15).
componente(5, 'i5',cpu, intel, 7, 15).
componente(6, 'Intel741',grafica, intel, 6, 30).

:-dynamic tipo/2.

componentes_tipo(LTipos):-
	findall(Tipo,componente(_,_,tipo,_,_,preco),ListaC),
	precomedio(ListaC,Conta,PrecoMedio),
	retractall(tipo(ListaC,PrecoMedio)),
	append(tipo(ListaC,PrecoMedio)).

precomedio([],0,0).
precomedio([H|T],Conta,Medio):-
	Conta is 1,
	precomedio(T,Conta1,Medio1),
	Medio is ((Medio+Medio1)/Conta1).

