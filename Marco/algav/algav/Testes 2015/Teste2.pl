pesquisa_viaturas(_,_,_,[],[],0).
pesquisa_viaturas(Dt_inicio,Dt_fim,Comb,[(Marca,Comb,Ano)|T],[(Marca,Ano)|L],N):-
	Dt_inicio <Ano,
	Dt_fim >= Ano,
	pesquisa_viaturas(Dt_inicio,Dt_fim,Comb,T,L,N1),
	N is N1+1.
pesquisa_viaturas(Dt_inicio,Dt_fim,Comb,[_|T],L,N):-
	pesquisa_viaturas(Dt_inicio,Dt_fim,Comb,T,L,N).


