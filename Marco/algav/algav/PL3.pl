/* -------------- 1 -------------- */

mulher(maria,30).
mulher(rosa,15).
mulher(teresa,21).
homem(pedro,18).
homem(tiago,43).

par(X,Y):- !,homem(X,_),
	mulher(Y,_).

mostra_par:- par(X,Y), write(X), write(' '), write(Y), nl,
 fail; true.

escolhe_par(X,Y,MAX,SOM):-
	homem(X,IX), mulher(Y,IY),
	SOM is IX+IY,
	SOM =< MAX.

/* ---------------- 2 ---------------*/

cor(c�u, azul).
cor(c�u, cinzento).
cor(c�u, preto).
cor(mar, azul).
cor(mar, verde).
cor(mar, cinzento).
cor(via, cinzenta).
cor(via, castanha).
cor(via, preta).

transporte(c�u,avi�o).
transporte(c�u,helic�ptero).
transporte(c�u,foguete).
transporte(mar,navio).
transporte(mar,lancha).
transporte(via,carro).
transporte(via,cami�o).
transporte(via,mota).
transporte(via,autocarro).

/* 2.1 */

nega(X):- call(X),
	!,fail;true.
/* 2.2 */

se_entao_senao(X,Y,Z):-(call(X),
	call(Y));call(Z).



/* 2.3 */

se_entao(X,Y):- call(X), call(Y);true.


/* 2.4 */

questao(Valor):-
	write('Qual � o Atributo?'),nl, read(R1),
	write('Qual � o objecto?'),nl, read(R2),
	repeat,
	write('Qual o valor do/da cor do/da '), write(R2), write('?'),nl, read(Valor),
	P=..[R1,R2,Valor], call(P),call(P).
