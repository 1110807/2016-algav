/* --- 1 --- */

p3(L,LI):-p3(L,[],LI).

p3([],LI,LI).
p3([H|T],L,LI):-

	(\+number(H),p3(T,[H|L],LI);
	 p3(T,L,LI)).


/* --- 3 --- */

p4(_,I,F,_):- F < I,!.
p4(L,I,F,SL):-
	p4(L,I,F,SL,1).

p4([H|_],_,F,[H],F).

p4([H|T],I,F,[H|T2],C):-
	(C == I; C > I, C < F),
	C1 is C+1,
	p4(T,I,F,T2,C1).

p4([_|T],I,F,SL,C):-
	C1 is C + 1,
	p4(T,I,F,SL,C1).
