%EX1
%Factos
%Homem
homem(manuel).
homem(anibal).
homem(jose).
homem(fernando).
homem(francisco).
homem(alvaro).
homem(carlos).
homem(paulo).
homem(ricardo).
homem(daniel).
homem(samuel).

%mulher
mulher(maria).
mulher(alzira).
mulher(marta).
mulher(diana).
mulher(celia).
mulher(delfina).
mulher(zulmira).
mulher(fatima).
mulher(sara).
mulher(adriana).

%casados
casados(manuel,maria).
casados(anibal,alzira).
casados(jose,marta).
casados(fernando,diana).
casados(alvaro,celia).
casados(carlos,zulmira).
casados(paulo,fatima).

%descendente direto
descendente_direto(marta,manuel,maria).
descendente_direto(fernando,manuel,maria).
descendente_direto(diana,anibal,alzira).
descendente_direto(francisco,anibal,alzira).
descendente_direto(alvaro,anibal,alzira).
descendente_direto(delfina,jose,marta).
descendente_direto(carlos,fernando,diana).
descendente_direto(paulo,fernando,diana).
descendente_direto(sara,alvaro,celia).
descendente_direto(ricardo,carlos,zulmira).
descendente_direto(daniel,carlos,zulmira).
descendente_direto(adriana,paulo,fatima).
descendente_direto(samuel,paulo,fatima).


descendente_direto(Filho,Progenitor):- descendente_direto(Filho,Progenitor,_).
descendente_direto(Filho,Progenitor):- descendente_direto(Filho,_,Progenitor).
%Regras

%EX2

%filho
filho(Filho,Progenitor):- homem(Filho),descendente_direto(Filho,Progenitor,_).
filho(Filho,Progenitor):- homem(Filho),descendente_direto(Filho,_,Progenitor).

%filha
filha(Filha,Progenitor):- mulher(Filha),(descendente_direto(Filha,Progenitor,_);descendente_direto(Filha,_,Progenitor)).

%pai
pai(Pai,Desc):- descendente_direto(Desc,Pai,_).

%mae
mae(Mae,Desc):- descendente_direto(Desc,_,Mae).

%av�
avoh(Pai,Desc):- pai(Pai,Desc2), descendente_direto(Desc,Desc2).

%av�
avom(Mae,Desc):- mae(Mae,Desc2), descendente_direto(Desc,Desc2).

%neto
neto(Neto,Avo):- filho(Neto,Prog), descendente_direto(Prog,Avo).

%neta
neta(Neta,Avo):- filha(Neta,Prog), descendente_direto(Prog,Avo).

%irm�o
irmao(Irmao,Irmao2):- homem(Irmao), descendente_direto(Irmao,Pai,Mae),descendente_direto(Irmao2,Pai,Mae), Irmao\==Irmao2.

/*irmao(Filho,Filho2):- filho(Filho,Progenitor), descendente_direto(Filho2,Progenitor), Filho\==Filho2.*/

%irma
irma(Irma,Irma2):- mulher(Irma), descendente_direto(Irma,Pai,Mae),descendente_direto(Irma2,Pai,Mae), Irma\==Irma2.

%tio de sangue
tio(Tio,Sobrinho):-  irmao(Tio,Irm) , descendente_direto(Sobrinho,Irm).

%tio direto
tio(Tio,Sobrinho):- casados(Tio,Tia), irma(Tia,Irm), descendente_direto(Sobrinho,Irm).

%tia de sangue
tia(Tia,Sobrinho):-  irma(Tia,Irm) , descendente_direto(Sobrinho,Irm).

%tia de direta
tia(Tia,Sobrinho):- casados(Tio,Tia), irmao(Tio,Irm), descendente_direto(Sobrinho,Irm).

%sobrinho
sobrinho(Sobrinho,Tio):- homem(Sobrinho), tio(Tio,Sobrinho).
sobrinho(Sobrinho,Tia):- homem(Sobrinho), tia(Tia,Sobrinho).

%sobrinha
sobrinha(Sobrinha,Tio):- mulher(Sobrinha), tio(Tio,Sobrinha).
sobrinha(Sobrinha,Tia):- mulher(Sobrinha), tia(Tia,Sobrinha).

%primo
primo(Primo,Primo2):- sobrinho(Primo,Tio), descendente_direto(Primo2,Tio).

%prima
prima(Prima,Prima2):- sobrinha(Prima,Tio), descendente_direto(Prima2,Tio).

%cunhado
cunhado(Cunhado,Cunhado2):- casados(Cunhado,Mulher), irma(Mulher,Cunhado2).
cunhado(Cunhado,Cunhado2):- casados(Cunhado,Mulher), irma(Mulher,Cunhado3),(casados(Cunhado3,Cunhado2); casados(Cunhado2,Cunhado3)).
cunhado(Cunhado,Cunhado2):- irmao(Cunhado,Cunhado3),(casados(Cunhado3,Cunhado2); casados(Cunhado2,Cunhado3)).

%cunhada
cunhada(Cunhada,Cunhada2):- casados(Homem,Cunhada), irmao(Homem,Cunhada2).
cunhada(Cunhada,Cunhada2):- casados(Homem,Cunhada), irmao(Homem,Cunhada3),(casados(Cunhada3,Cunhada2); casados(Cunhada2,Cunhada3)).
cunhada(Cunhada,Cunhada2):- irma(Cunhada,Cunhada3),(casados(Cunhada3,Cunhada2); casados(Cunhada2,Cunhada3)).

%EX3
ascendente(Pessoa,L):- descendente(L,Pessoa).
/*
ascendente(Pessoa,L):- descendente_direto(Pessoa,L).
ascendente(Pessoa,L):- descendente_direto(Pessoa,Temp),ascendentes(Temp,L).

*/

descendente(Pessoa,L):- descendente_direto(L,Pessoa).
descendente(Pessoa,L):- descendente_direto(L,P1),
			descendente(Pessoa,P1).

%EX5
isep(B,B).

%EX6
homens(joao).
homens(miguel).
homens(antonio).
homens(marco).
homens(jose).
homens(joaquim).
homens(manuel).

mulheres(maria).
mulheres(joana).
mulheres(susana).
mulheres(ana).
mulheres(joaquina).

ouro(miguel).
ouro(joana).
ouro(jos�).

carro(ana).
carro(joao).

cabelo(joao,loiro).
cabelo(jose,loiro).
cabelo(joaquim,loiro).
cabelo(miguel,preto).
cabelo(antonio,preto).
cabelo(manuel,preto).

cabelo(joana,loiro).
cabelo(susana,loiro).
cabelo(joaquina,loiro).
cabelo(maria,morena).
cabelo(ana,morena).

rica(Pessoa) :- ouro(Pessoa);(parentes(Pessoa,P,_);parentes(Pessoa,_,P),ouro(P)).

gostos(joao,Pessoa):- rica(Pessoa),cabelo(Pessoa,loiro).
gostos(miguel,Pessoa):- rica(Pessoa),cabelo(Pessoa,morena).
gostos(maria,Pessoa):- cabelo(Pessoa,preto).
gostos(joana,Pessoa):- rica(Pessoa),cabelo(Pessoa,preto).

gosta(H,M) :- homens(H), mulheres(M), gostos(H,M).
gosta(M,H) :- mulheres(H), homens(M), gostos(M,H).

gostoMutuo(H,M):- gosta(H,M),gosta(M,H).

%EX7

parentes(marco,jose,joaquina).
parentes(jose,joaquim,_).
parentes(joaquim,manuel,_).

mestico(Pessoa,M,P):- (parentes(Pessoa,P,_),parentes(Pessoa,_,M),cabelo(P,C),cabelo(M,C1), C == C1),( parentes(P,P2,_); parentes(M,P2,_) , cabelo(P2,C3), C3\==C).



