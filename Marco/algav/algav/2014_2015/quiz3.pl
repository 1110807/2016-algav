/*quiz1*/
/*m�todo proximos-alinea1*/
proximos(A,C,T,L):-findall( (Ct,[S,A|T]), ( (liga(A,S,Cx);liga(S,A,Cx)), not(member(S,T)), Ct is C+Cx), L).
pesquisa(Origem,Destino,Perc,C):-pesquisa1([(0,[Origem])],Destino,Perc,C).
pesquisa1([(C,[D|T])|_],D,L,C):-reverse([D|T],L),!.
/*adicao do sort - alinea2*/
pesquisa1([(C,[A|T])|R],D,P,Ct):-proximos(A,C,T,L),append(R,L,N),sort(N,NL),pesquisa1(NL,D,P,Ct).

/*quiz2*/
/*proximo - Alinea 1*/
proximos(A,T,Max,L,C):-findall( ([S,A|T],Cx),((caminho(A,S,Cy);caminho(S,A,Cy)), Max>=Cy, Cx is C+Cy),L).
pesquisa(Origem,Destino,Max,Perc):-pesquisa1([([Origem],0)],Destino,Max,Perc).
pesquisa1([([D|T],_)|_],D,Max,L):-reverse([D|T],L),!.
/*adicao do findall - alinea2*/
pesquisa1([([A|T],C)|R],D,Max,P):-proximos(A,T,Max,L,C),append(R,L,NL),
findall(([X|TT],Cx), (member(([X|TT],Cx),NL), Cx<8), Cf),
pesquisa1(Cf,D,Max,P).
