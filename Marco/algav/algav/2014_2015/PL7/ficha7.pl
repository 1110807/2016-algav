:- use_module(library(statistics)). % time/1

%Exercicio 1
ramo(a,b).
ramo(a,d).
ramo(a,e).
ramo(b,a).
ramo(b,c).
ramo(b,f).
ramo(c,b).
ramo(c,f).
ramo(d,a).
ramo(d,e).
ramo(d,g).
ramo(e,a).
ramo(e,d).
ramo(e,f).
ramo(f,b).
ramo(f,c).
ramo(f,e).
ramo(f,h).
ramo(g,d).
ramo(g,h).
ramo(h,f).
ramo(h,g).


vertices(LV):- findall([A,B],ramo(A,B),LR),
	       append(LR,LV,LV). %problema deve estar no append!

hamilton(Orig,Path):- caminhos(Orig,Orig,Path),
	              vertices(LV),
		      equalset(LV,Path).

caminhos(X,Y,P):- todos_cam(X,Y,[X],P).

todos_cam(Y,Y,_,[Y]).
todos_cam(X,Y,L,[X|P]):-
	ramo(X,Z),
	\+ member(Z,L),
	todos_cam(Z,Y,[Z|L],P).

equalset(P,N):- subSet(P,N),
	        subSet(N,P).

subSet([],_).
subSet([H|T],Y):- member(H,Y),
	           subSet(T,Y).

%----------------------------
%Exercicio 2
liga(a,b).
liga(a,c).
liga(a,d).
liga(b,e).
liga(b,f).
liga(c,f).
liga(c,g).
liga(d,a).
liga(d,g).
liga(d,h).
liga(d,i).
liga(e,j).
liga(f,a).
liga(f,j).
liga(f,k).
liga(g,f).
liga(g,o).
liga(g,h).
liga(h,d).
liga(h,l).
liga(i,l).
liga(j,m).
liga(j,n).
liga(k,n).
liga(k,p).
liga(l,p).

%Alinea A
resolve_emprof(Orig,Dest,Caminho):- time(visitaprof(Dest,[[Orig]],Caminho)).

visitaprof(Dest,[[Dest|T]|_],Cam):- reverse([Dest|T],Cam).%inverte o caminho actual
visitaprof(Dest,[[H|T]|Outros],Cam):-
			    findall([X,H|T],(Dest\==H,liga(H,X),\+ member(X,[H|T])),Novos),
			    append(Outros,Novos,Todos),
			    write(Todos),nl,
			    visitaprof(Dest,Todos,Cam).

%Resolucao de aula
resolve_prof2(Origem,Dest,Sol):- time(visitaprof2(Origem,Dest,[Origem],Sol)).

visitaprof2(Dest,Dest,_,[Dest]).
visitaprof2(Orig,Dest,LV,[Orig|L]):-liga(Orig,X), %(liga(Orig,X);liga(X,Orig)) - bidirec.
				\+ member(X,LV),
	                         visitaprof2(X,Dest,[X|LV],L).

%resolve_prof_niveis
resolve_prof_niveis(Origem,Dest,Nivel,Sol):-
				  time(visitaprof_nivel(Origem,Dest,[Origem],Nivel,Sol)).

visitaprof_nivel(Dest,Dest,_,_,[Dest]).
visitaprof_nivel(Orig,Dest,LV,Nivel,[Orig|L]):-
	                                     Nivel1 >=0,Nivel1 is Nivel-1,
					     liga(Orig,X),
					     \+ member(X,LV),
					     visitaprof_nivel(X,Dest,[X|LV],Nivel1,L).

%Alinea C
resolve_emlarg(Orig,Dest,Caminho):- time( visitalarg(Dest,[[Orig]],Caminho) ).

visitalarg(Dest,[[Dest|T]|_],Cam):- reverse([Dest|T],Cam).%inverte o caminho actual
visitalarg(Dest,[[H|T]|Outros],Cam):-
			    findall([X,H|T],(Dest\==H,liga(H,X),\+ member(X,[H|T])),Novos),
			    append(Novos,Outros,Todos), %Ao trocar a ordem do Novos n�s com                                                        %os outros,executa a profundidade
							%em vez de largura
							%-> append(Outros,Novos,Todos),
			    write(Todos),nl,
			    visitalarg(Dest,Todos,Cam).

%Resolucao de aula
resolve_larg2(Orig,Dest,Sol):-
	time((visitalarg2([[Orig]],Dest,Cam),reverse(Cam,Sol))).

visitalarg2([[Dest|Cam]|_],Dest,[Dest|Cam]).
visitalarg2([Cam|OutrosCam],Dest,Sol):- estender(Cam,NovosCam),
					    append(OutrosCam,NovosCam,Caminhos),
					    write('Novo Caminho: '),write(Caminhos),nl,
					    visitalarg2(Caminhos,Dest,Sol).

estender([No|Cam],NovosCam):-findall([NovoNo,No|Cam],
				     (liga(No,NovoNo),\+member(NovoNo,[No|Cam]))
				     ,NovosCam).
