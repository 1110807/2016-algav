
%-------------------------------------------------
% BASE DE FACTOS
% homem/1
% mulher/1
% casados/2
% geram/3
%-------------------------------------------------

%homem(HOMEM)
homem(manuel).
homem(anibal).
homem(jose).
homem(fernando).
homem(francisco).
homem(alvaro).
homem(carlos).
homem(paulo).
homem(ricardo).
homem(daniel).
homem(samuel).

%mulher(MULHER)
mulher(maria).
mulher(alzira).
mulher(marta).
mulher(diana).
mulher(celia).
mulher(delfina).
mulher(zulmira).
mulher(fatima).
mulher(sara).
mulher(adriana).

%casados(HOMEM,MULHER)
casados(manuel,maria).
casados(anibal,alzira).
casados(jose,marta).
casados(fernando,diana).
casados(alvaro,celia).
casados(carlos,zulmira).
casados(paulo,fatima).

%geram(HOMEM,MULHER,DESCENDENTE)
geram(manuel,maria,marta).
geram(manuel,maria,fernando).
geram(anibal,alzira,diana).
geram(anibal,alzira,francisco).
geram(anibal,alzira,alvaro).
geram(jose,marta,delfina).
geram(fernando,diana,carlos).
geram(fernando,diana,paulo).
geram(alvaro,celia,sara).
geram(carlos,zulmira,ricardo).
geram(carlos,zulmira,daniel).
geram(paulo,fatima,adriana).
geram(paulo,fatima,samuel).

 
%-------------------------------------------------
% BASE DE REGRAS
%-------------------------------------------------

%-------------------------------------------------
% Predicados auxiliares
%-------------------------------------------------

%progenitor(P,D)
progenitor(P,D):- geram(P,_,D).
progenitor(P,D):- geram(_,P,D).	

%conjuge(X,Y):- casados(X,Y).
conjuge(X,Y):- casados(X,Y).
conjuge(X,Y):- casados(Y,X).


%-------------------------------------------------
% Rela��es familiares
%-------------------------------------------------

%filho(FILHO,PROGENITOR)
filho(F,P):-	homem(F),
			progenitor(P,F).

%filha(FILHA,PROGENITOR)
filha(F,P):-	mulher(F),
			progenitor(P,F).

%pai(PAI,CRIA)
pai(P,F):-	homem(P),
		progenitor(P,F).

%mae(MAE,CRIA)
mae(P,F):-	mulher(P),
		progenitor(P,F).

%av�(AV�,NETO(A))
avoH(X,Y):-	pai(X,Z),
		progenitor(Z,Y).

%av�(AV�,NETO(A))
avoM(X,Y):-	mae(X,Z),
		progenitor(Z,Y).

%neto(NETO,AV�(�))
neto(X,Y):-	filho(X,Z),
		progenitor(Y,Z).

%neta(NETO,AV�(�))
neta(X,Y):-	filha(X,Z),
		progenitor(Y,Z).

%irmao(IRMAO,IRMA(O))
irmao(X,Y):-	homem(X),
			geram(P,M,X),
			geram(P,M,Y),
			X\==Y.

%irma(IRMA,IRMA(O))
irma(X,Y):-	mulher(X),
		geram(P,M,X),
		geram(P,M,Y),
		X\==Y.

%tio(TIO,SOBRINHO(A))
%---tio de sangue
tio(X,Y):-	irmao(X,Z),
		progenitor(Z,Y).

%---tio de lei
tio(X,Y):-	casados(X,I),
		irma(I,Z),
		progenitor(Z,Y).


%tia(TIA,SOBRINHO(A))
%---tia de sangue
tia(X,Y):-	irma(X,Z),
		progenitor(Z,Y).

%---tia de lei
tia(X,Y):-	casados(I,X),
		irmao(I,Z),
		progenitor(Z,Y).


%sobrinho(SOBRINHO,TIO(A))
sobrinho(X,Y):-	homem(X),
			tia(Y,X).

sobrinho(X,Y):-	homem(X),
			tio(Y,X).

%sobrinha(SOBRINHA,TIO(A))
sobrinha(X,Y):-	mulher(X),
			tia(Y,X).

sobrinha(X,Y):-	mulher(X),
			tio(Y,X).

%primo(PRIMO,PRIMO(A))
primo(X,Y):-	filho(X,Z),
			tio(Z,Y).


%prima(PRIMA,PRIMO(A))
prima(X,Y):-	filha(X,Z),
			tio(Z,Y).

%cunhado(CUNHADO,CUNHADO(A))
%---conjunge da irma---
cunhado(X,Y):-	irma(Z,Y),
			conjuge(X,Z).

%---irmao do conjunge---
cunhado(X,Y):-	irmao(X,Z),
			conjuge(Z,Y). 

%---marido da irma do conjuge---
cunhado(X,Y):-	conjuge(X,I),
			irma(I,C),
			conjuge(C,Y).


%cunhada(CUNHADA,CUNHADO(A))
%---conjunge da irmao---
cunhada(X,Y):-	irmao(Z,Y),
			conjuge(X,Z).

%---irma do conjunge---
cunhada(X,Y):-	irma(X,Z),
			conjuge(Z,Y). 

%---esposa do irmao do conjuge---
cunhada(X,Y):-	conjuge(X,I),
			irmao(I,C),
			conjuge(C,Y).


%---descendente---
descendente(X,Y):-	progenitor(Y,X).

descendente(X,Y):-	progenitor(Y,Z),
				descendente(X,Z).


%---ascendente---
ascendente(X,Y):- 	descendente(Y,X).

%---EOF


			
