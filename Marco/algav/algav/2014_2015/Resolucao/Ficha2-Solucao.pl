%-----------------------------------------------
% Exerc�cio 1
%-----------------------------------------------

% Potencia de um numero de expoente inteiro
%potencia(Base,Expoente,Resultado)
potencia(_,0,1):-	!.

potencia(X,Y,Z):-	Y<0,
			!,
			potencia(X,-Y,Z1),
			Z is 1/Z1.

potencia(X,Y,Z):-	Y1 is Y-1,
			potencia(X,Y1,Z1),
			Z is Z1*X.

% Factorial de um numero
%factorial(Numero,Resultado)
factorial(0,1):-	!.
factorial(X,Y):-	X1 is X-1,
			factorial(X1,Y1),
			Y is Y1*X.

% Divisao inteira entre dois numeros
%divisao(Dividendo,Divisor,Quociente)
divisao(X,Y,0):-	X<Y,!.
divisao(X,Y,Z):-	X1 is X-Y, 
		divisao(X1,Y,Z1), 
		Z is Z1+1.

% Resto da divisao inteira
%resto(Dividendo,Divisor,Resto)
resto(X,Y,X):-	X<Y,!.
resto(X,Y,Z):-	X1 is X-Y, 
		resto(X1,Y,Z).


%-----------------------------------------------
% Exerc�cio 2
%-----------------------------------------------
liga(a,b).
liga(a,c).
liga(a,d).
liga(b,e).
liga(b,f).
liga(c,f).
liga(c,g).
liga(d,g).
liga(d,h).
liga(d,i).
liga(e,j).
liga(f,j).
liga(f,k).
liga(g,f).
liga(g,o).
liga(g,h).
liga(h,l).
liga(i,l).
liga(j,m).
liga(j,n).
liga(k,n).
liga(k,p).
liga(l,p).

%-----------------------------------------------
% vers�o 1: caminho invertido
%-----------------------------------------------
caminho(O, O):-
	write(O), 
	tab(2).

caminho(O, D):-
	liga(O, X),
	caminho(X, D),
	write(O), 
	tab(2).



% vers�o 2: caminho sem invers�o
caminho1(O, D):-
	caminho2(D, O).

caminho2(O, O):-
	write(O), tab(2).

caminho2(O, D):-
	liga(X, O),
	caminho2(X, D),
	write(O), 
	tab(2).

% vers�o 3: caminho sem invers�o
caminho3(O, O):-
	write(O), 
	tab(2).

caminho3(O, D):-
	liga(X, D),
	caminho3(O, X),
	write(D), 
	tab(2).


%-----------------------------------------------
% Exerc�cio 3
%-----------------------------------------------

hanoi(N):-move(N,esquerda,centro,direita,0,_).


move(0,_,_,_,I,I):-!.
move(N,A,B,C,I,I2):-M is N-1,
		move(M,A,C,B,I,I1),
		J is I1+1,
		informa(A,B,J),
		move(M,C,B,A,J,I2).


informa(A,B,I):- 
		write(I),
		write(': MOVER DISCO DE '),
		write(A),
		write(' PARA '),
		write(B),
		nl.

%-----------------------------------------------
% Explica��o para a necessidade do 
% contador ter 2 argumentos:
%-----------------------------------------------

p(X):-
	q(X),
	write(X),nl.

q(X):-
	X1 is X+1,
	write(X1),nl.

p1(X):-
	q1(X,X1),
	write(X1),nl.

q1(X,X1):-
	X1 is X+1,
	write(X1),nl.

%-----------------------------------------------
% Vers�o que mostra descri��o 
% dos passos identada
%-----------------------------------------------

hanoi2(N):-move2(N,esquerda,centro,direita,0).


move2(0,_,_,_,_):-!.
move2(N,A,B,C,Ident):-M is N-1,
		Ident1 is Ident+1,
		informa_move2(M,A,C,B,Ident),
		move2(M,A,C,B,Ident1),
		informa2(A,B,Ident),
		informa_move2(M,C,B,A,Ident),
		move2(M,C,B,A,Ident1).

informa2(A,B,Ident):- 
		tab(Ident),
		write('MOVER DISCO DE '),
		write(A),
		write(' PARA '),
		write(B),
		nl.

informa_move2(0,_,_,_,_):-!.
informa_move2(M,A,B,C,Ident):-
	tab(Ident),
	write('Mover '),
	write(M),
	write(' da haste '),
	write(A),
	write(' para a haste '),
	write(B),
	write(' usando a haste '),
	write(C),
	write(' como auxiliar'),
	nl.


