%------Base de conhecimento------
n_clientes(a,900000).
n_clientes(b,1000000).
n_medio_chamadas_por_cliente(a,10).
n_medio_chamadas_por_cliente(b,8).
duracao_media_chamadas_por_cliente(a,2).
duracao_media_chamadas_por_cliente(b,1.7).
custo_por_unidade_tempo(a,40).
custo_por_unidade_tempo(b,55).
custos_operacao_por_cliente(a,500).
custos_operacao_por_cliente(b,550).
custos_com_marketing(a,0).
custos_com_marketing(b,0).


estado(E,[P1,P2,P3,P4,P5,P6]):-
	n_clientes(E,P1),
	n_medio_chamadas_por_cliente(E,P2),
	duracao_media_chamadas_por_cliente(E,P3),
	custo_por_unidade_tempo(E,P4),
	custos_operacao_por_cliente(E,P5),
	custos_com_marketing(E,P6).


%consequencia(a1,[r1,r2,r7]).
consequencia(a1,[Pa1,Pa2,Pa3,Pa4,Pa5,Pa6],[Pa1,NPa2,NPa3,NPa4,Pa5,Pa6],[Pb1,Pb2,Pb3,Pb4,Pb5,Pb6],[Pb1,Pb2,Pb3,Pb4,Pb5,Pb6]):-
	NPa2 is Pa2*1.1,
	NPa3 is Pa3*1.1,
	NPa4 is Pa4*0.9.

%consequencia(a2,[r1,r4,r6]).
consequencia(a2,[Pa1,Pa2,Pa3,Pa4,Pa5,Pa6],[NPa1,NPa2,Pa3,Pa4,Pa5,NPa6],[Pb1,Pb2,Pb3,Pb4,Pb5,Pb6],[Pb1,Pb2,Pb3,Pb4,Pb5,Pb6]):-
	NPa2 is Pa2*1.1,
	NPa1 is Pa1+100,
	NPa6 is Pa6+9000000.

%consequencia(a3,[r1,r3,r5]).
consequencia(a3,[Pa1,Pa2,Pa3,Pa4,Pa5,Pa6],[NPa1,NPa2,Pa3,Pa4,NPa5,Pa6],[Pb1,Pb2,Pb3,Pb4,Pb5,Pb6],[NPb1,Pb2,Pb3,Pb4,Pb5,Pb6]):-
	NPa2 is Pa2 * 1.1,
	NPa1 is Pa1+5000,NPb1 is Pb1-5000,
	NPa5 is Pa5*1.02.



lucro(Valor, [P1,P2,P3,P4,P5,P6]):- 
				Valor is P1*P2*P3*P4-P1*P5-P6.


minimax(Nivel,EmpresaA, EmpresaB,EstadoA, EstadoB, LucroA, LucroB, LA):-
		minimax(1,Nivel,EmpresaA, EmpresaB, EstadoA, LucroA, EstadoB, LucroB, LA).


%--- n�s folha ---
minimax(Nivel,Nivel,EmpresaA,EmpresaB,MEstadoA, MLucroA, MEstadoB, MLucroB, MLA):-
		estado(EmpresaA,EstA),
		estado(EmpresaB,EstB),
		findall((ValorAX, ValorBX, NEstAX, NEstBX,[AX]),
				(
					consequencia(AX,EstA,NEstAX, EstB, NEstBX), 
					lucro(ValorAX, NEstAX), 
					lucro(ValorBX, NEstBX)
				), LEstados),
		bestof(LEstados, MLucroA, MLucroB, MEstadoA, MEstadoB, MLA).


%--- n�s intermedios ---
minimax(Nivel1,Nivel2,EmpresaA,EmpresaB,MEstadoA, MLucroA, MEstadoB, MLucroB, LA):-
		Nivel1<Nivel2,
		NewNivel is Nivel1+1,
		findall((ValorNAX, ValorNBX, NNEstAX, NNEstBX,[AX|LA]),
				(minimax(NewNivel, Nivel2, EmpresaB,EmpresaA,NEstBX, _, NEstAX, _,LA),
				 consequencia(AX,NEstAX,NNEstAX,NEstBX, NNEstBX),
                                lucro(ValorNAX, NNEstAX),
                                lucro(ValorNBX, NNEstBX)
				),LNEstados),
		bestof(LNEstados,MLucroA, MLucroB, MEstadoA, MEstadoB, LA).



%---ordena e extrai o ultimo elemento---
bestof(LEstados,MLucroA, MLucroB,MEstadoA, MEstadoB, MLA):-
	sort(LEstados,LS),
	append(_,[( MLucroA, MLucroB,MEstadoA, MEstadoB,MLA)],LS),!.
