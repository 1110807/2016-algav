%arco(Nodo1,Nodo2)
arco(a,d).
arco(f,c).
arco(b,c).
arco(b,f).
arco(a,b).
arco(a,e).
arco(e,f).
arco(h,f).
arco(e,d).
arco(g,d).
arco(g,h).
arco(d,a).
arco(c,f).
arco(c,b).
arco(f,b).
arco(b,a).
arco(e,a).
arco(f,e).
arco(f,h).
arco(d,e).
arco(d,g).
arco(h,g).

%---Alinea 1---
%circuito(NodoPartida,Caminho)

circuito(Nodo,Cam):-
	%calcular todos os nodos
	setof(X,X^Y^(arco(X,Y);arco(Y,X)),L),

	%Remover o nodo de partida da lista
	remove(Nodo,L,LR),

	%contar o numero de nodos
	length(LR,N),
	
	circuito(Nodo,[Nodo],Cam,N).


%circuito(NodoPartida,NodosVisistados,Caminho,NumNodosAVisitar)

circuito(Nodo,[H|T],Cam,0):- 	
					%condi�ao necessaria p/caminho fechado
					arco(H,Nodo),
					%inverte o caminho actual
					reverse([H|T],Cam).

circuito(Nodo,[H|T],Cam,N):-	
					N>0,
					arco(H,X),
					not member(X,[H|T]),
					N1 is N-1,
					circuito(Nodo,[X,H|T],Cam,N1).
		

%---Alinea 2---

liga(a,b).
liga(a,c).
liga(a,d).
liga(b,e).
liga(b,f).
liga(c,f).
liga(c,g).
liga(d,a).
liga(d,g).
liga(d,h).
liga(d,i).
liga(e,j).
liga(f,a).
liga(f,j).
liga(f,k).
liga(g,f).
liga(g,o).
liga(g,h).
liga(h,d).
liga(h,l).
liga(i,l).
liga(j,m).
liga(j,n).
liga(k,n).
liga(k,p).
liga(l,p).

%-------------------------Pesquisa em largura-----------------------------------
bfs(Orig,Dest,Caminho):-	bfs2(Dest,[[Orig]],Caminho).


bfs2(Dest,[[Dest|T]|_],Cam):- 	
					%inverte o caminho actual
					reverse([Dest|T],Cam).

bfs2(Dest,[[H|T]|Outros],Cam):-	
					findall([X,H|T],(Dest\==H,liga(H,X),not member(X,[H|T])),Novos),
					append(Outros,Novos,Todos),
					write(Todos),nl,
					bfs2(Dest,Todos,Cam).

/*
| ?- bfs(a,g,C).
[[b,a],[c,a],[d,a]]
[[c,a],[d,a],[e,b,a],[f,b,a]]
[[d,a],[e,b,a],[f,b,a],[f,c,a],[g,c,a]]
[[e,b,a],[f,b,a],[f,c,a],[g,c,a],[g,d,a],[h,d,a],[i,d,a]]
[[f,b,a],[f,c,a],[g,c,a],[g,d,a],[h,d,a],[i,d,a],[j,e,b,a]]
[[f,c,a],[g,c,a],[g,d,a],[h,d,a],[i,d,a],[j,e,b,a],[j,f,b,a],[k,f,b,a]]
[[g,c,a],[g,d,a],[h,d,a],[i,d,a],[j,e,b,a],[j,f,b,a],[k,f,b,a],[j,f,c,a],[k,f,c,a]]
C = [a,c,g] ;

[[g,d,a],[h,d,a],[i,d,a],[j,e,b,a],[j,f,b,a],[k,f,b,a],[j,f,c,a],[k,f,c,a]]
C = [a,d,g] ;
*/

	
%-------------------------Pesquisa em profundidade-----------------------------------

dfs(Orig,Dest,Caminho):-	dfs2(Dest,[[Orig]],Caminho).


dfs2(Dest,[[Dest|T]|_],Cam):- 	
					%inverte o caminho actual
					reverse([Dest|T],Cam).

dfs2(Dest,[[H|T]|Outros],Cam):-	
					findall([X,H|T],(Dest\==H,liga(H,X),not member(X,[H|T])),Novos),
					%append(Outros,Novos,Todos),
					append(Novos,Outros,Todos),
					write(Todos),nl,
					dfs2(Dest,Todos,Cam).

/*
| ?- dfs(a,g,C).
[[b,a],[c,a],[d,a]]
[[e,b,a],[f,b,a],[c,a],[d,a]]
[[j,e,b,a],[f,b,a],[c,a],[d,a]]
[[m,j,e,b,a],[n,j,e,b,a],[f,b,a],[c,a],[d,a]]
[[n,j,e,b,a],[f,b,a],[c,a],[d,a]]
[[f,b,a],[c,a],[d,a]]
[[j,f,b,a],[k,f,b,a],[c,a],[d,a]]
[[m,j,f,b,a],[n,j,f,b,a],[k,f,b,a],[c,a],[d,a]]
[[n,j,f,b,a],[k,f,b,a],[c,a],[d,a]]
[[k,f,b,a],[c,a],[d,a]]
[[n,k,f,b,a],[p,k,f,b,a],[c,a],[d,a]]
[[p,k,f,b,a],[c,a],[d,a]]
[[c,a],[d,a]]
[[f,c,a],[g,c,a],[d,a]]
[[j,f,c,a],[k,f,c,a],[g,c,a],[d,a]]
[[m,j,f,c,a],[n,j,f,c,a],[k,f,c,a],[g,c,a],[d,a]]
[[n,j,f,c,a],[k,f,c,a],[g,c,a],[d,a]]
[[k,f,c,a],[g,c,a],[d,a]]
[[n,k,f,c,a],[p,k,f,c,a],[g,c,a],[d,a]]
[[p,k,f,c,a],[g,c,a],[d,a]]
[[g,c,a],[d,a]]
C = [a,c,g] ;

[[d,a]]
[[g,d,a],[h,d,a],[i,d,a]]
C = [a,d,g] ;
*/

