/***********
Exerc�cio 1a
***********/

elimina1(_,[],[]).
elimina1(X,[X|T],T):-!.
elimina1(X,[Y|T],[Y|T1]):-elimina1(X,T,T1).

/***********
Exerc�cio 1b
***********/

elimina_todos(_,[],[]).
elimina_todos(X,[X|T],T1):-!,elimina_todos(X,T,T1).
elimina_todos(X,[Y|T],[Y|T1]):-elimina_todos(X,T,T1).

elimina_todos2(X,L,L):-not member(X,L),!.
elimina_todos2(X,L,L1):-elimina1(X,L,L2),!,elimina_todos2(X,L2,L1).

/**********
Exerc�cio 2
**********/

p([],[]).
p(L,[X|L1]):-elimina2(X,L,Li),p(Li,L1).

elimina2(X,[X|L],L).
elimina2(X,[Y|L],[Y|M]):-elimina2(X,L,M).

/**********
Exerc�cio 3
**********/

substitui(_,_,[],[]).
substitui(X,Y,[X|T],[Y|T1]):-
	!,substitui(X,Y,T,T1).
substitui(X,Y,[H|T],[H|T1]):-
	substitui(X,Y,T,T1).

/**********
Exerc�cio 4
**********/

insere(_,P,_,_):-P<1,!,fail.
insere(X,1,L,[X|L]):-!.
insere(X,P,[H|T],[H|T1]):-
	P1 is P-1,
	insere(X,P1,T,T1).

/**********
Exerc�cio 5a
**********/











/**********
Exerc�cio 5b
**********/

/*
A fun��o do segundo argumento de inverte1 � ir acumulando os sucessivos elementos que
v�o aparecendo � cabe�a da lista que aparece no primeiro argumento. Os elementos que
aparecem � cabe�a da lista do primeiro argumento v�o sendo empurrados para a lista
do segundo argumento. Quando a lista do primeiro argumento fica vazia, a lista do
segundo argumento cont�m a lista inicial invertida.
*/

/**********
Exerc�cio 6a
**********/

uni�o([],L,L).
uni�o([X|L],L1,L2):-member(X,L1),!,uni�o(L,L1,L2).
uni�o([X|L],L1,[X|L2]):-uni�o(L,L1,L2).





/**********
Exerc�cio 6b
**********/

/*
Em respostas alternativas, obtidas atrav�s de ;, elementos que fossem comuns �s duas listas
dos dois primeiros argumentos poderiam aparecer repetidos na lista uni�o.
*/

/**********
Exerc�cio 6c
**********/

interseccao([],L,[]).
interseccao([X|R],Y,[X|Z]):-member(X,Y),!,interseccao(R,Y,Z).
interseccao([_|R],Y,Z):-interseccao(R,Y,Z).

/**********
Exerc�cio 6d
**********/

diferenca([],L2,L2).
diferenca([X|L1],L2,D):-member(X,L2),!,del_all(X,L2,L21),diferenca(L1,L21,D).
diferenca([X|L1],L2,[X|D]):-diferenca(L1,L2,D).
del_all(_,[],[]).
del_all(X,[X|L],L1):-!,del_all(X,L,L1).
del_all(X,[H|L],[H|L1]):-del_all(X,L,L1).

/**********
Exerc�cio 7
**********/

totobola([],[]).
totobola([L|T],[X|T1]):-
	list(L),!,member(X,L),
	totobola(T,T1).
totobola([X|T],[X|T1]):-
	totobola(T,T1).

/**********
Exerc�cio 8
**********/

totoloto(L,L1):- totoloto(L,L1,6).
totoloto(_,[],0):-!.
totoloto([X|T],[X|T1],N):- N1 is N-1, totoloto(T,T1,N1).
totoloto([_|T],T1,N):- totoloto(T,T1,N).

/**********
Exerc�cio 9
**********/

datas_jogo([],_,[]).
datas_jogo([H|L1],L2,L):-
	inters(H,L2,L3),
	datas_jogo(L1,L2,L4),
	append(L3,L4,L).

inters(_,[],[]).
inters(Int1,[Int2|T],[Int|T1]):-
	inters1(Int1,Int2,Int),!,
	inters(Int1,T,T1).
inters(Int1,[_|T],L):-
	inters(Int1,T,L).

inters1(t(I1,F1),t(I2,F2),_):-
	(I2>F1;I1>F2),!,fail.
inters1(t(I1,F1),t(I2,F2),t(I,F)):-
	max(I1,I2,I),
	min(F1,F2,F).

max(I1,I2,I1):-I1>=I2,!.
max(_,I2,I2).
min(F1,F2,F1):-F1=<F2,!.
min(_,F2,F2).
