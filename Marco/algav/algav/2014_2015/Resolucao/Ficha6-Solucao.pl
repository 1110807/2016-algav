% Exerc�cio 1

liga(a,b).
liga(a,c).
liga(a,d).
liga(d,a).
liga(b,e).
liga(b,f).
liga(c,f).
liga(c,g).
liga(d,g).
liga(d,h).
liga(d,i).
liga(e,j).
liga(f,a).
liga(f,j).
liga(f,k).
liga(g,f).
liga(g,o).
liga(g,h).
liga(h,d).
liga(h,l).
liga(i,l).
liga(j,m).
liga(j,n).
liga(k,n).
liga(k,p).
liga(l,p).

% Al�nea a)

go(Orig,Dest,L):-go(Orig,Dest,[Orig],L).

go(Dest,Dest,_,[Dest]).
go(Orig,Dest,LA,[Orig|L]):-
	liga(Orig,X),
	not member(X,LA),
	go(X,Dest,[X|LA],L).

% Al�nea b)

gera_todos(Orig,Dest,LL):-
	findall(L,go(Orig,Dest,L),LL).

% al�nea c)

menor(Orig,Dest,L):-
	gera_todos(Orig,Dest,LL),
	menor(LL,L).

menor([L1|LT],M):-
	menor(LT,M),
	length(L1,C1),
	length(M,CM),
	CM<C1,!.
menor([L1|_],L1).

% Primeiro em largura

go4(Orig,Dest,Perc):- go1([[Orig]],Dest,P), inverte(P,Perc).

go1([Prim|Resto],Dest,Prim):- Prim=[Dest|_].
go1([[Dest|T]|Resto],Dest,Perc):-
	!,
	go1(Resto,Dest,Perc).
go1([[Ult|T]|Outros],Dest,Perc):-
	findall([Z,Ult|T],proximo_no(Ult,T,Z),Lista),
	% append(Lista,Outros,NPerc), pesquisa primeiro em profundidade
	append(Outros,Lista,NPerc), % pesquisa primeiro em largura
	go1(NPerc,Dest,Perc).

proximo_no(X,T,Z):-
	liga(X,Z),
	not member(Z,T).

inverte(L,LI):-inverte(L,[],LI).
inverte([],LI,LI).
inverte([H|T],L,LI):-inverte(T,[H|L],LI).

% Exerc�cio 2

:-dynamic cruza/3,esta��o_linhas/2,esta��es/1.


linha(1,[a,b,c,d,e,f]).
linha(2,[g,b,h,i,j,k]).
linha(3,[l,j,m,n,o,d,p]).
linha(4,[f,q,r,s,t]).
linha(5,[t,u,j,v,a,g]).


inicializa:- gera_cruzamentos,gera_esta��es,gera_esta��es_linhas.

gera_cruzamentos:-findall(_,cruzamento,_).

cruzamento:-	linha(N1,LE1),
		linha(N2,LE2),
		N1\==N2,
		intersec��o(LE1,LE2,[H|T]),
		assertz(cruza(N1,N2,[H|T])).
%		intersec��o(LE1,LE2,LI),
%		assertz(cruza(N1,N2,LI)).

gera_esta��es:-	findall(L,linha(_,L),LE),
		une(LE,LEsta��es),
		asserta(esta��es(LEsta��es)).

une([LH|LT],LU):-une(LT,LU1),uni�o(LH,LU1,LU).
une([],[]).


gera_esta��es_linhas:- 
  findall(_,
	  (esta��es(LE),member(E,LE),todas_linhas(E,LL),assertz(esta��o_linhas(E,LL))),
	  _).

todas_linhas(E,LL):-
	findall(Linha,(linha(Linha,LEL),member(E,LEL)),LL).

gera_caminho(E1,E2,LC):-
		esta��o_linhas(E1,LE1),esta��o_linhas(E2,LE2),caminho(E1,LE1,E2,LE2,[],LC).


caminho(E1,LE1,E2,LE2,_,[(E1,E2,Linha)]):-intersec��o(LE1,LE2,[H|T]),member(Linha,[H|T]),!.
caminho(E1,LE1,E2,LE2,LLV,[(E1,EI,Linha)|LC]):-member(Linha,LE1),
					(not member(Linha,LLV)),
					cruza(Linha,_,L),
					member(EI,L),
					esta��o_linhas(EI,LEI),
					caminho(EI,LEI,E2,LE2,[Linha|LLV],LC).

mtl(E1,E2,LMTL):-findall(L,gera_caminho(E1,E2,L),LL),menor(LL,LMTL).

menor([H],H).
menor([H|T],H):-menor(T,L1),length(H,C),length(L1,C1),C<C1,!.
menor([H|T],L1):-menor(T,L1).

uni�o([],L,L).
uni�o([X|L],L1,L2):-member(X,L1),!,uni�o(L,L1,L2).
uni�o([X|L],L1,[X|L2]):-uni�o(L,L1,L2).

escreve([H|T]):-
	write(H),nl,escreve(T).
escreve([]).

intersec��o([],_,[]).
intersec��o([X|L],L1,[X|L2]):-member(X,L1),!,intersec��o(L,L1,L2).
intersec��o([_|L],L1,L2):-intersec��o(L,L1,L2).



