%---------------------------------------------------------
%Exercicio 1
%---------------------------------------------------------

mulher(maria,30).
mulher(rosa,15).
mulher(teresa,21).
homem(pedro,18).
homem(tiago,43).

par(X,Y):- homem(X,IX),
	mulher(Y,IY).

mostra_par:- par(X,Y), write(X), write(' '), write(Y), nl,
	fail; true.
	
	%Alinea 1.a 6
	
	
%Alinea b.1-1
par1(X,Y):- homem(X,IX),
	mulher(Y,IY),!.

mostra_par1:- par1(X,Y), write(X), write(' '), write(Y), nl,
	fail; true.

%Alinea b.2 - altera so mulheres
par2(X,Y):- homem(X,IX),!,
	mulher(Y,IY).

mostra_par2:- par2(X,Y), write(X), write(' '), write(Y), nl,
	fail; true.

%Alinea b.3 - 6
par3(X,Y):- !,homem(X,IX),
	mulher(Y,IY).

mostra_par3:- par3(X,Y), write(X), write(' '), write(Y), nl,
	fail; true.

%Alinea d
escolhe_par(_,_,MAX,_):- MAX < 36,!.
escolhe_par(X,Y,MAX,SOM):-
	homem(X,IX),
	mulher(Y,IY),
	SOM is IX+IY,
	SOM =< MAX.


%---------------------------------------------------------
%Exercicio 2
%---------------------------------------------------------

cor(c�u, azul).
cor(c�u, cinzento).
cor(c�u, preto).
cor(mar, azul).
cor(mar, verde).
cor(mar, cinzento).
cor(via, cinzenta).
cor(via, castanha).
cor(via, preta).


transporte(c�u,avi�o).
transporte(c�u,helic�ptero).
transporte(c�u,foguete).
transporte(mar,navio).
transporte(mar,lancha).
transporte(via,carro).
transporte(via,cami�o).
transporte(via,mota).
transporte(via,autocarro).

%Alinea 1
nega(X):-call(X),!,fail;true.

%Alinea 2
se_ent�o_sen�o(X,CE,CS):- call(X),!,call(CE);
			  call(CS).
/*
			  
se:- write(ok),nl,write('...'),nl.
sn:- write(falhou),nl,write('...'),nl.

se_entao_senao(P,E,_):-P,!,E.
se_entao_senao(_,_,S):-S.

*/
%Alinea 3
se_ent�o(X,CE):-call(X),!,call(CE);
		true.

/* Resolucao da aula
se_entao(P,E):-P,!,E.
se_entao(P,_):- nega(P).
*/

%Alinea 4
quest�o(Valor):-write('Qual � o atributo? '),
		read(Atributo),
		write('Qual � o objecto? '),
		read(Objecto),
		Lista=..[Atributo,Objecto,Valor],
		repeat,
		write('Qual � o valor do/da '),
		write(Atributo),
		write(' do/da '),
		write(Objecto),
		write('? '),
		read(Valor),
		call(Lista).


/*Resolucao da aula*/

questao:-write('Qual � o atributo? '),
		read(Atributo),
		write('Qual � o objecto? '),
		read(Objecto),
		questao(Atributo,Objecto).

questao(Atributo,Objecto):- repeat,
		write('Qual � o valor do/da '),
		write(Atributo),
		write(' do/da '),
		write(Objecto),
		write('?'),
		read(Valor),
		Lista=..[Atributo,Objecto,Valor],
		call(Lista),
		write('valor='),write(Valor),nl.





