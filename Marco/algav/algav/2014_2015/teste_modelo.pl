%Prova Modelo
homem(joao,43,'Vila Real').
homem(pedro,31,'Porto').
homem(rui,25,'Caldas de Aregos').
homem(emanuel,14,'Venda de Raparigas').

mulher(teresa,25,'Porto').
mulher(maria,33,'Porto').
mulher(sofia,13,'Cinfaes do Douro').

select(Pessoa,Local,Max):-
	(homem(Pessoa,Idade,Local);mulher(Pessoa,Idade,Local)),
	Idade<Max.
%Ex c - Admitindo que o determinado valor seja 30 anos
select1(Pessoa,Local,Idade):-
	(homem(Pessoa,Idade,Local);mulher(Pessoa,Idade,Local)),
	Idade<30.



%Prova 1 do ano passado

%livro(codigo,tipo,titulo,npags)
livro(1,romance, 'Triunfo dos Porcos', 128).
livro(2,policial, 'Morte no Nilo', 168).
livro(3,ensaio, 'Sentados na Relva', 174).
livro(4,romance, 'Jangada de Pedra', 350).
livro(5,policial, 'Crime no Expresso do Oriente', 288).

%autor(nome_autor,c�digo_livro).
autor('Jose Saramago',4).
autor('George Orwell',1).
autor('Agatha Christie',5).
autor('Agatha Christie',2).
autor('Fernando Namora',3).


%ex3
publicacao(Nome,Livro):- livro(Codigo,_,Livro,_),
			 autor(Nome,Codigo).
