:- use_module(library(statistics)). % time/1

%Exercicio 1

%cidade(Cidade,X,Y)
cidade(a,45,95).
cidade(b,90,95).
cidade(c,15,85).
cidade(d,40,80).
cidade(e,70,80).
cidade(f,25,65).
cidade(g,65,65).
cidade(h,45,55).
cidade(i,5,50).
cidade(j,80,50).
cidade(l,65,45).
cidade(m,25,40).
cidade(n,55,30).
cidade(o,80,30).
cidade(p,25,15).
cidade(q,80,15).
cidade(r,55,10).

%estrada(Cidade1,Cidade2,Distancia).
estrada(a,b,45).
estrada(a,c,32).
estrada(a,d,16).
estrada(a,e,30).
estrada(b,e,25).
estrada(d,e,30).
estrada(c,d,26).
estrada(c,f,23).
estrada(c,i,37).
estrada(d,f,22).
estrada(f,h,23).
estrada(f,m,25).
estrada(f,i,25).
estrada(i,m,23).
estrada(e,f,48).
estrada(e,g,16).
estrada(e,j,32).
estrada(g,h,23).
estrada(g,l,20).
estrada(g,j,22).
estrada(h,m,25).
estrada(h,n,27).
estrada(h,l,23).
estrada(j,l,16).
estrada(j,o,20).
estrada(l,n,19).
estrada(l,o,22).
estrada(m,n,32).
estrada(m,p,25).
estrada(n,p,34).
estrada(n,r,20).
estrada(o,n,25).
estrada(o,q,15).
estrada(p,r,31).

%---------------------------------Algoritmo primeiroOMelhor-----------

primeiroOMelhor(Origem,Destino,Solucao,Custo):-
		time(primeiroOMelhor2(Destino,[Origem],Solucao,Custo)).


primeiroOMelhor2(Destino,[Destino|T],Solucao,0):-
				reverse([Destino|T],Solucao).

primeiroOMelhor2(Destino,[H|T],Solucao,Custo):-
			Destino\==H,
			findall((CE,CX,[X,H|T]),(estrada(H,X,CX),\+ member(X,[H|T]),estimativa(X,Destino,CE)),Novos),
			sort(Novos,LS),
			LS=[(_,CMX,Melhor)|_],
			primeiroOMelhor2(Destino,Melhor,Solucao,C),
			Custo is C+CMX.


estimativa(N1,N2,Valor):-
			cidade(N1,X1,Y1),
			cidade(N2,X2,Y2),
			distancia(X1,Y1,X2,Y2,Valor).

distancia(X1,Y1,X2,Y2,Distancia):-
			Distancia is sqrt((X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2)).



%-----------------Algoritmo Branch and Bound--------------------------

branchAndBound(Origem,Destino,Solucao,Custo):-
			time(branchAndBound2(Destino,[(0,[Origem])],Solucao,Custo)).


branchAndBound2(Destino,[(Custo,[Destino|T])|_],Solucao,Custo):-
					reverse([Destino|T],Solucao).

branchAndBound2(Destino,[(Custo,[H|T])|Resto],Solucao,CustoSol):-
			findall((C,[X,H|T]),(Destino\==H,estrada(H,X,CX),\+ member(X,[H|T]),C is CX+Custo),Novos),
			append(Novos,Resto,Todos),
			sort(Todos,LS),
			write(LS),nl,
			branchAndBound2(Destino,LS,Solucao,CustoSol).

%-----------------Algoritmo A*--------------------------

aStar(Origem,Destino,Solucao,Custo):-
			estimativa(Origem,Destino,Estimativa),
			time(aStar2(Destino,[(Estimativa,0,[Origem])],Solucao,Custo)).


aStar2(Destino,[(_,Custo,[Destino|T])|_],Solucao,Custo):-
					reverse([Destino|T],Solucao).

aStar2(Destino,[(_,Custo,[H|T])|Resto],Solucao,CustoSol):-
			findall((CEC,C,[X,H|T]),
					(	Destino\==H,
						estrada(H,X,CX),
						\+ member(X,[H|T]),
						C is CX+Custo,
						estimativa(X,Destino,Est),
						CEC is Est + C),
					Novos),
			append(Novos,Resto,Todos),
			sort(Todos,LS),
			write(LS),nl,
			aStar2(Destino,LS,Solucao,CustoSol).
