%Quest�o aula 2 - vers�o 1
%Ex 1
% Apresentar os numeros pares e deve ignorar os numeros que n�o s�o
% numericos
p1([]).
p1([H|T]):- (number(H), R is H mod 2, R == 0, write(H), p1(T)); p1(T),!.

%Apresenta todos os numeros
p1s([]).
p1s([H|T]):- (number(H), write(H), p1s(T)); p1s(T),!.

%Ex 2
%p2([a,b,b,b,c,c,a,a,f,b,b],L).
%L = [b,c,a,b]
p2(L,LR):-p2(L,[],LR).
p2([],L,L):-!.
p2([X,Y|T],[],LR):-X==Y,p2([Y|T],[X],LR),!.
p2([X,Y|T],[HR|TR],LR):-X==Y,X\==HR,!,p2([Y|T],[X,HR|TR],LR).
p2([_|T],L,LR):-p2(T,L,LR).

%Quest�o aula 2 - vers�o 2
%Ex 1
% Apresentar apenas valors n�o numericos
p3(L,LI):-p3(L,[],LI).
p3([],LI,LI):-!.
p3([H|T],L,LI):-not(number(H)),!,p3(T,[H|L],LI).
p3([_|T],L,LI):-p3(T,L,LI).

%Ex 2
%p4([a,b,c,d,e,f,g,h],2,5,SL).
%SL = [b,c,d,e]
p4(L,N,M,LR):-p4(L,1,N,M,LR).
p4(_,X,_,M,[]):-X>M,!.
p4([H|T],X,N,M,[H|LR]):-X>=N,X=<M,!,X1 is X+1,p4(T,X1,N,M,LR).
p4([_|T],X,N,M,LR):-X<N,X1 is X+1,p4(T,X1,N,M,LR).
