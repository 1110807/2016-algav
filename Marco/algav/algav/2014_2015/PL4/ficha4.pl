%Ex1
membro(X,[X|_]).
membro(X,[_|L]):- membro(X,L).

%Ex2
concatena([],L,L).
concatena([A|B],C,[A|D]):- concatena(B,C,D).

%Ex3

%Alinea A
%conta(Lista,Numero).
conta([],0).
conta([_|T],N):- conta(T,N1),
	         N is N1+1.

%Alinea B
%soma(Lista,Soma).
soma([],0).
soma([H|T],S):-	number(H),
		soma(T,S1),
		S is S1+H.

%Alinea C
%media(Lista,Media).
media([],_):- !,fail.
media(L,M):-soma(L,S),
	    conta(L,C),
	    M is S/C.

%OU
media2(L,M):- media2(L,0,0,M).
media2([],S,C,M):- M is S/C.
media2([H|T],S,C,M):-	S1 is S+H,
			C1 is C+1,
			media2(T,S1,C1,M).
%OU
media3(L,M):- media3(L,_,_,M).
media3([],0,0,0).
media3([H|T],C,S,M):- media3(T,K,A,_),
	              C is K+1,
		      S is A+H,
		      M is S/C.

%Alinea D
%minimo(Lista,Minimo).
minimo([H|T],M):- minimo(T,M),
		  M<H,!.
minimo([H|_],H).

%OU
minimo2([M],M).
minimo2([H1,H2|T],M):-	H1<H2,!,
			minimo2([H1|T],M).
minimo2([_|T],M):-minimo2(T,M).

%OU
minimo3(A,B,A):- A =<B,!.
minimo3(_,B,B).

menor_list([H],H).
menor_list([H|T],M):- menor_list(T,MT),
	              minimo3(H,MT,M).

%Alinea E
%contarN(Lista,Minimo,Maximo,Resultado). - Nao esta a funcionar
contarN([],_,_,0).
contarN([H|T],Min,Max,R):-
	(Min<H , Max>H),
	contarN(T,Min,Max,R1),
		R is R1+1.

%Ex4
%lista(Lista).
lista(L):- var(L),!,fail.

lista([]).
lista([_|T]):- lista(T).
