%Exercicio 1
%-------------------------------------------------------

%Alinea a
%potencia(Base,Expoente,Resultado).
potencia(_,0,1):-!.

potencia(B,E,R):- E<0, !,
	potencia(B,-E,R1),
	R is 1/R1.

potencia(B,E,R):- E1 is E-1, potencia(B,E1,R1), R is B*R1.

%Alinea b
%factorial(Numero,Resultado).
factorial(0,1):-!.

factorial(N,R):- N1 is N-1, factorial(N1,R1), R is N*R1.


%Alinea c
%divisao(Dividendo,Divisor,Quociente).
divisao(D,Div,0):- D <Div,!.

divisao(D,Div,Q):- D1 is D-Div,
	divisao(D1,Div,Q1),
	Q is Q1+1.

%Alinea d
%resto(Dividendo,Divisor,Resto).
resto(D,Div,D):- D<Div,!.

resto(D,Div,R):- D1 is D-Div,
	resto(D1,Div,R).


%-------------------------------------------------------
%Exercicio 2
%-------------------------------------------------------

liga(a,b).
liga(a,c).
liga(a,d).
liga(b,e).
liga(b,f).
liga(c,f).
liga(c,g).
liga(d,g).
liga(d,h).
liga(d,i).
liga(e,j).
liga(f,j).
liga(f,k).
liga(g,f).
liga(g,h).
liga(g,o).
liga(h,l).
liga(i,l).
liga(j,m).
liga(j,n).
liga(k,n).
liga(k,p).
liga(l,p).

%caminho(Origem,Destino).
caminho(O,O):- write(O).

caminho(O,D):- liga(O,D1),
	caminho(D1,D),
	write(O), tab(2).

%vers�o 2: caminho sem invers�o
caminho1(O, D):-
	caminho2(D, O).

caminho2(O, O):-
	write(O), tab(2).

caminho2(O, D):-
	liga(O1, O),
	caminho2(O1, D),
	write(O),tab(2).

% vers�o 3: caminho sem invers�o
caminho3(O, O):-
	write(O),
	tab(2).

caminho3(O, D):-
	liga(O1, D),
	caminho3(O, O1),
	write(D),
	tab(2).

%-------------------------------------------------------
%Exercicio 3
%-------------------------------------------------------

%hanoi(Numero).
hanoi(N):-move(N,esquerda,centro,direita,0,_).

move(0,_,_,_,I,I):-!.
move(N,A,B,C,I,I2):-M is N-1,
		move(M,A,C,B,I,I1),
		J is I1+1,
		informa(A,B,J),
		move(M,C,B,A,J,I2).

informa(A,B,I):-
		write(I),
		write(': MOVER DISCO DE '),
		write(A),
		write(' PARA '),
		write(B),
		nl.


%-------------------------------------------------------
%Exercicio 4
%-------------------------------------------------------

primo(N):- primo(N,N).

primo(_,1):- write('� primo').

primo(_,2):- write('� primo').

primo(N,R):- N1 is N-1, resto(N,N1,R).





