% Exerc�cio 1

liga(a,b).
liga(a,c).
liga(a,d).
liga(d,a).
liga(b,e).
liga(b,f).
liga(c,f).
liga(c,g).
liga(d,g).
liga(d,h).
liga(d,i).
liga(e,j).
liga(f,a).
liga(f,j).
liga(f,k).
liga(g,f).
liga(g,o).
liga(g,h).
liga(h,d).
liga(h,l).
liga(i,l).
liga(j,m).
liga(j,n).
liga(k,n).
liga(k,p).
liga(l,p).
/*
liga(a,b).
liga(a,c).
liga(b,a).
liga(b,c).
liga(b,d).
liga(c,a).
liga(c,d).
*/
%Alinea A - funciona para grafos n�o bidirecionais

cam(Orig,Dest,L):-cam(Orig,Dest,[Orig],L).

cam(Dest,Dest,_,[Dest]).
cam(Orig,Dest,LA,[Orig|L]):-
	liga(Orig,X),
	\+ member(X,LA),
	cam(X,Dest,[X|LA],L).

%Resolucao aula - Grafo bidirecional
camcc(O,D):- cam_aux(O,D,[D]).

cam_aux(O,O,[O|_]):- write(O),tab(2).
cam_aux(O,D,L):- liga(X,D), \+ member(X,L),
	cam_aux(O,X,[X|L]),
	write(D),tab(2).

%Alinea B
gera_caminhos(Orig,Dest,LL):-
	findall(L,cam(Orig,Dest,L),LL).

%Resolucao aula
caminhos(X,Y,P):- todos_cam(X,Y,[X],P).
todos_cam(Y,Y,_,[Y]).

todos_cam(X,Y,L,[X|P]):-
	liga(X,Z),
	\+ member(Z,L),
	todos_cam(Z,Y,[Z|L],P).


%Alinea C
menor(Orig,Dest,L):-
	gera_caminhos(Orig,Dest,LL),
	menor(LL,L).

menor([L1|LT],M):-
	menor(LT,M),
	length(L1,C1),
	length(M,CM),
	CM<C1,!.
menor([L1|_],L1).

%Exercicio 2

:-dynamic cruza/3,esta��o_linhas/2,esta��es/1.

linha(1,[a,b,c,d,e,f]).
linha(2,[g,b,h,i,j,k]).
linha(3,[l,j,m,n,o,d,p]).
linha(4,[f,q,r,s,t]).
linha(5,[t,u,j,v,a,g]).

inicializa:- gera_cruzamentos,gera_esta��es,gera_esta��es_linhas.

limpeza:- retractall(cruza(A,B,C)), retractall(esta��es(A)), retractall(esta��o_linhas(A,B)).



gera_cruzamentos:-findall(_,cruzamento,_).

cruzamento:- linha(N1,LE1),
	linha(N2,LE2),
	N1 \== N2,
 intersec��o(LE1,LE2,[H|T]),
 assertz(cruza(N1,N2,[H|T])).
/*
 Ao colocar LI nos dois predicados contiua a aparecer listas vazias
  intersec��o(LE1,LE2,LI),
  assertz(cruza(N1,N2,LI)).

Se colocarmos [H|T] no local de LI elimina as listas vazias nos
resultados
  intersec��o(LE1,LE2,[H|T]),
  assertz(cruza(N1,N2,[H|T])).

 */

%Alinea B
gera_esta��es:-	findall(L,linha(_,L),LE),une(LE,LEsta��es),asserta(esta��es(LEsta��es)).

une([LH|LT],LU):-une(LT,LU1),uni�o(LH,LU1,LU).
une([],[]).

%Alinea C
gera_esta��es_linhas:-
  findall(_,(esta��es(LE),member(E,LE),todas_linhas(E,LL),assertz(esta��o_linhas(E,LL))),
	  _).

todas_linhas(E,LL):-
	findall(Linha,(linha(Linha,LEL),member(E,LEL)),LL).

%Alinea D
gera_caminho(E1,E2,LC):- esta��o_linhas(E1,LE1),
	                 esta��o_linhas(E2,LE2),caminho(E1,LE1,E2,LE2,[],LC).


caminho(E1,LE1,E2,LE2,_,[(E1,E2,Linha)]):-intersec��o(LE1,LE2,[H|T]),member(Linha,[H|T]),!.

caminho(E1,LE1,E2,LE2,LLV,[(E1,EI,Linha)|LC]):-member(Linha,LE1),(\+ member(Linha,LLV)),
					cruza(Linha,_,L),member(EI,L),
					esta��o_linhas(EI,LEI),
					caminho(EI,LEI,E2,LE2,[Linha|LLV],LC).

%Alinea E
menos_trocas_linha(E1,E2,LMTL):-findall(L,gera_caminho(E1,E2,L),LL),menor(LL,LMTL).

%Predicados auxiliares:
intersec��o([],_,[]).
intersec��o([X|L],L1,[X|L2]):-member(X,L1),!,intersec��o(L,L1,L2).
intersec��o([_|L],L1,L2):-intersec��o(L,L1,L2).

uni�o([],L,L).
uni�o([X|L],L1,L2):-member(X,L1),!,uni�o(L,L1,L2).
uni�o([X|L],L1,[X|L2]):-uni�o(L,L1,L2).




















