/* --- 1 --- */

p1([]).
p1([H|T]):-
	(number(H),par(H), write(H),!,p1(T);
	p1(T)).

par(N):- N mod 2 =:= 0.


/* --- 2 --- */

p2(L,L1):-
	p2(L,L1,_,_).

p2([],[],_,_).

p2([H|T],[H|T2],Piloto,Valor):-
	Valor == 0,
	Piloto == H,
	Valor1 = 1,
	p2(T,T2,Piloto,Valor1).

p2([H|T],L,Piloto,Valor):-
	(Valor == 1,Piloto == H,p2(T,L,Piloto,Valor),!;
	 Valor1 = 0,p2(T,L,H,Valor1),!).
