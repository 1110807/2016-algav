elimina(_,[],[]).
elimina(X,[X|L],L):-!.
elimina(X,[Y|L],[Y|R]):-
	elimina(X,L,R).

eliminall(_,[],[]).
eliminall(X,[X|L],R):-!, eliminall(X,L,R).
eliminall(X,[Y|L],[Y|R]):-
	eliminall(X,L,R).

substitui(_,_,[],[]).
substitui(X,Y,[X|L],[Y|R]):-!,substitui(X,Y,L,R).
substitui(X,Y,[Z|R],[Z|T]):-
	substitui(X,Y,R,T).
