elimina(_,[],[]).
elimina(X,[X|R],R):-!.
elimina(X,[Z|R],[Z|R1]):-
	elimina(X,R,R1).

eliminaTodos(_,[],[]).
eliminaTodos(X,[X|R],R1):-eliminaTodos(X,R,R1),!.
eliminaTodos(X,[Z|R],[Z|R1]):-
	eliminaTodos(X,R,R1).

substitui(_,_,[],[]).
substitui(X,Y,[X|R],[Y|R1]):-substitui(X,Y,R,R1),!.
substitui(X,Y,[Z|R],[Z|R1]):-
	substitui(X,Y,R,R1).

insere(X,1,R,[X|R]):-!.
insere(X,P,[Z|R],[Z|R1]):-
	P1 is P-1,
	insere(X,P1,R,R1).

uniao([],L,L).
uniao([X|T],L,LR):-
	member(X,L),!,
	uniao(T,L,LR).
uniao([H|T],L,[H|LR]):-
	uniao(T,L,LR).

intersecao([],_,[]).
intersecao([X|T],L,[X|LR]):-
	member(X,L),!,
	intersecao(T,L,LR).
intersecao([_|T],L,LR):-
	intersecao(T,L,LR).


