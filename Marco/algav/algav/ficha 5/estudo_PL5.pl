elimina(_,[],[]).
elimina(X,[X|T],T):-!.
elimina(X,[H|T1],[H|T2]):-
	elimina(X,T1,T2).

eliminaTodos(_,[],[]).
eliminaTodos(X,[X|R1],R2):-!,eliminaTodos(X,R1,R2).
eliminaTodos(X,[H|T1],[H|T2]):-
	eliminaTodos(X,T1,T2).

substitui(_,_,[],[]).
substitui(X,Y,[X|R],[Y|R]):-!.
substitui(X,Y,[Z|R1],[Z|R2]):-
	substitui(X,Y,R1,R2).

insere(_,_,[],[]).
insere(X,1,R,[X|R]):-!.
insere(X,P,[Z|R1],[Z|R2]):-
	P1 is P-1,
	insere(X,P1,R1,R2).

