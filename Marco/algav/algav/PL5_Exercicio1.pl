elimina(_,[],[]).
elimina(X,[X|R],R):-!.
elimina(X,[Y|R1], [Y|R2]):-
	elimina(X,R1,R2).

elimina_todos(_,[],[]).
elimina_todos(X,[X|R1],R2):-!, elimina_todos(X,R1,R2).
elimina_todos(X,[Y|R1], [Y|R2]):-
	elimina_todos(X,R1,R2).

substituir(_,_,[],[]).
substituir(X,Y,[X|R1],[Y|R2]):-!,substituir(X,Y,R1,R2).
substituir(X,Y,[Z|R1],[Z|R2]):-
	   substituir(X,Y,R1,R2).

inserir(_,_,[],[]).
inserir(X,1,R,[X|R]).
inserir(X,N,[Y|R1],[Y|R2]):-
	N1 is N-1,
	inserir(X,N1,R1,R2).

interseccao([],_,[]).
interseccao([X|R1],R2,[X,R3]):-
	member(X,R2),
	interseccao(R1,R2,R3).
interseccao([_,R1],R2,R3):-
	interseccao(R1,R2,R3).

diferenca([],_,[]).
diferenca([X|R1],R2,[X,R3]):-
	not(member(X,R2)),
	interseccao(R1,R2,R3).
diferenca([_,R1],R2,R3):-
	interseccao(R1,R2,R3).

totobola([],[]).
totobola([X|R1],[Y|R2]):-
	is_list(X),!,
	member(Y,X),
	totobola(R1,R2).
totobola([X|R1],[X|R2]):-
	totobola(R1,R2).

totoloto(0,_,[]).
totoloto(N,[X|R1],[X|R2]):-
	N1 is N-1,
	totoloto(N1,R1,R2).
totoloto(N,[_|R1],R2):-
	totoloto(N,R1,R2).






