/*
Teorica
1.(P∧Q)∧~(P∨Q)
2.[]
3.Nenhuma das anteriores é válida (N*M)
4.Nenhuma das anteriores
5.Apenas as afirmações (i) e (iii) são verdadeiras
6. ?
7.O valor que chega à raiz da arvore é exatamente o mesmo quando se
aplica o minimax puro ou minimax com cortes alfa-beta
*/
%Teorica alinea 2
p([],_,[]).
p([X|L1],L2,[X|LI]):- member(X,L2),p(L1,L2,LI).
p([_|L1],L2,LI):- p(L1,L2,LI).

%Pratica

%Alinea 1
salto(N,(Y,X),(X1,Y1)):-( X1 is X+2, Y1 is Y+1,X1=<N,Y1=<N,X1>=1,Y1>=1);
                        ( X1 is X+1, Y1 is Y+2,X1=<N,Y1=<N,X1>=1,Y1>=1);
                        ( X1 is X+2, Y1 is Y-1,X1=<N,Y1=<N,X1>=1,Y1>=1);
                        ( X1 is X+1, Y1 is Y-2,X1=<N,Y1=<N,X1>=1,Y1>=1);
			( X1 is X-2, Y1 is Y+1,X1=<N,Y1=<N,X1>=1,Y1>=1);
                        ( X1 is X-1, Y1 is Y+2,X1=<N,Y1=<N,X1>=1,Y1>=1);
			( X1 is X-2, Y1 is Y-1,X1=<N,Y1=<N,X1>=1,Y1>=1);
                        ( X1 is X-1, Y1 is Y-2,X1=<N,Y1=<N,X1>=1,Y1>=1).

%Alinea 2
visita(N,(X,Y),L):- visita1(N,(X,Y),[(X,Y)],L).

visita1(N,_,L,Sol):- T is N*N, length(L,K), K==T, reverse(L,Sol).
visita1(N,(X,Y),L,Sol):- salto(N,(X,Y),D),\+member(D,L),
	                 visita1(N,D,[D|L],Sol).

%Alinea 3
percurso(N,(Y,X),L2):- findall(L,visita(N,(X,Y),L),L1), percurso1(N,L1,(X,Y),L2).

percurso1(N,[H|T],O,L1):- reverse(H,H1),
	(( percurso2(N,H1,O,L),reverse(H1,T),append(T,L,L1));
	percurso1(N,T,O,L1)).
percurso2(N,[H|_],O,[O]):- salto(N,H,O).
