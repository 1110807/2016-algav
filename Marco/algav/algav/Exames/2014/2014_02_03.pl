/*
Teorica
1. � poss�vel provar que S � verdade por refuta��o.
2.Anulada.
3.Em G' a restricao {op2 precede op3} passa a ser redundante.
4.Quando dispomos de espa�o e mem�ria limitado ao efetuar uma pesquisa
em principio � preferivel optar pelo primeiro em profundidade em
detrimento do primeiro em largura.
5.11101011
6.Se as iteracoes sao complexas ?
7.

*/
%Pratica

%rede de postos de fabrico: dstposto(postA,postB,dist) (sentido Bid.)
dstposto(pt1,pt2,21).
dstposto(pt3,pt4,55).
dstposto(pt5,pt3,30).
dstposto(pt2,pt5,19).
dstposto(pt4,pt2,13).
dstposto(pt1,pt6,28).

%Abastecimentos robots : abstrobot(robot,lista_postos_abastecidos)
abstrobot(r1,[pt4,pt5,pt2,pt6,pt5]).
abstrobot(r2,[pt1,pt3,pt6]).
abstrobot(r3,[pt2,pt5,pt3,pt2,pt5,pt6,pt1,pt5]).

%Alinea 1
abastposto(P,Lfinal):- findall((Robot,Total),
			  ((dstposto(P,_,_);dstposto(_,P,_)),
			   abstrobot(Robot,LPosto),
			   conta(P,LPosto,0,Total)),L),
			   sort(L,Lfinal).

conta(_,[],Total,Total).
conta(Posto,[Posto|Percurso],Total,TotalFinal):-
	TotalAux is Total + 1,
	conta(Posto,Percurso,TotalAux,TotalFinal),!.
conta(Posto,[_|Percurso],Total,TotalFinal):-
	conta(Posto,Percurso,Total,TotalFinal).

%Alinea 2
cammin(Origem,Destino,Solucao,Custo):-
			cammin2(Destino,[(0,[Origem])],Solucao,Custo).

cammin2(Destino,[(Custo,[Destino|T])|_],Solucao,Custo):-
					reverse([Destino|T],Solucao).

cammin2(Destino,[(Custo,[H|T])|Resto],Solucao,CustoSol):-
			findall((C,[X,H|T]),(Destino\==H,
			(dstposto(H,X,CX);dstposto(X,H,CX)),
			\+ member(X,[H|T]),C is CX+Custo),Novos),
			append(Novos,Resto,Todos),
			sort(Todos,LS),
			cammin2(Destino,LS,Solucao,CustoSol).

%Alinea 3

desempenhorobot(R,P,D):- abstrobot(R,Lista),desempenhaux(Lista,D),
	               sort(Lista,LF),length(LF,P).

desempenhaux([X,Y|T],Num):- cammin(X,Y,_,D),desempenhaux([Y|T],Num1),
	                  Num is Num1+D,!.
desempenhaux(_,0).
