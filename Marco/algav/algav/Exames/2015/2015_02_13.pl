%ve(VeiculoA,VeiculoB,QoS) - VeiculoA consegue comunicar com VeiculoB
% com uma qualidade de sinal QoS. O valor de QoS � um inteiro entre 1 e 4
ve(v1,v2,1).
ve(v1,v3,3).
ve(v2,v4,1).
ve(v2,v5,2).
ve(v2,v7,2).
ve(v3,v5,1).
ve(v3,v6,4).
ve(v5,v7,2).
ve(v5,v8,1).
ve(v6,v7,3).

% retransmissor(ListaVeiculosRetransmissores) - lista de veiculos
% capazes de retransmitir informacao. Todos os outros apenas recebem.
retransmissor([v1,v2,v3,v4,v5,v6]).

% empresa(DEmpresa,ListaVeiculos) - relaciona a empresa em DEmpresa com
% a lista de veiculos que utilizam o seu servi�o.
empresa(a,[v1,v3,v4]).
empresa(b,[v2,v5,v6,v7]).
empresa(c,[v8]).

%Alinea 1
% retrans_emp(L):-
% findall((Empresa,Num),(retransmissor(LR),empresa(Empresa,LV),ve(H,T,_),
% member(H,LR), \+member(T,LV), Num is Num1 +1),L),retrans_empAux(.

retrans_emp(LF):- empresa(Empresa,LV),retransmissor(LR),
	retransaux(Empresa,LV,LR,LF,0).

retransaux(Empresa,[H|T],LR,LF,Num):- findall((Empresa,Num),((ve(H,V,_);ve(V,H,_)),member(V,LR),\+member(V,[H|T])),LF),
	retransaux(Empresa,T,LR,LF,Num1), Num is Num1 +1,!.


conta([],_,0).
conta([H|T],LR,Num):-!, ve(H,V,_), member(H,LR),\+member(V,[H|T]),conta(T,LR,Num1),Num is Num1 +1,!.

conta2([H|T],LV,Num):-ve(H,V,_), member(H,LR), \+member(V,LV), conta(T,LR,Num1), Num is Num1 + 1,! .
conta2([],_,0).


%Alinea 2

caminho_qos(Origem,Destino,Solucao,Custo):-Max is 5,
		caminho_qos2(Destino,[(Max,[Origem])],Max,Solucao,Custo).

caminho_qos2(Destino,[(Custo,[Destino|T])|_],_,Solucao,Custo):-
					reverse([Destino|T],Solucao),!.

caminho_qos2(Destino,[(Custo,[H|T])|Resto],Max,Solucao,CustoSol):-
			findall((C,[X,H|T]),(Destino\==H,
			(ve(H,X,CX);ve(X,H,CX)),
			\+ member(X,[H|T]),
		        ((CX<Custo, C is CX); C is Custo),C =< Max),Novos),
			append(Novos,Resto,Todos),
			sort(Todos,LS),
			caminho_qos2(Destino,LS,Max,Solucao,CustoSol).

%Alinea 3
