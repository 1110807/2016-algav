/*
Teorica
1.(b v c) => a
2.nenhuma das anteriores
3.o n�mero de planos de G2 � no m�ximo igual ao n�mero de planos de G1
4.Nenhuma das anteriores
5.a solu��o �ptima do problema poder� ser atingida, mas n�o h� garantia que o seja
6.As afirma��es (ii) e (iv) s�o verdadeiras
7.A aplica��o do algoritmo minimax retorna para a raiz da �rvore
exatamente o mesmo valor, do que quando se aplica o minimax com cortes
alfa e beta
*/

%Pratica
% recursos(tipo,listaRecursos)
recursos(doenca,[doenca1,doenca2,doenca3]).
recursos(subAtiva,[subAtiva1,subAtiva2,subAtiva3,subAtiva4,subAtiva5]).
recursos(documento,[documento1,documento2,documento3]).
recursos(medicamento,[medicamentoX,medicamentoY,medicamentoZ,medicamentoK,medicamentoH,medicamentoB,medicamentoH]).
recursos(contraIndicacao,[contraA1,contraA2,contraA3]).
% relacao(recurso, relacao, listaRecursos)
relacao(doenca1,temMedicacao,[subAtiva1,subAtiva2,subAtiva3]).
relacao(doenca2,temMedicacao,[subAtiva2,subAtiva4,subAtiva5]).
relacao(doenca3,temMedicacao,[subAtiva5]).
relacao(documento1,menciona,[doenca1,doenca2,doenca3,contraA2]).
relacao(documento2,menciona,[doenca2,doenca3,contraA3,contraA1]).
relacao(documento3,menciona,[doenca2,medicamentoK]).
relacao(subAtiva1,estaPresenteEm,[medicamentoX,medicamentoY,medicamentoZ]).
relacao(subAtiva2,estaPresenteEm,[medicamentoK]).
relacao(subAtiva3,estaPresenteEm,[medicamentoY,medicamentoK]).
relacao(subAtiva4,estaPresenteEm,[medicamentoA,medicamentoB]).
relacao(subAtiva5,estaPresenteEm,[medicamentoH]).
relacao(medicamentoX,temContraIndicacao,[contraA1]).
relacao(medicamentoY,temContraIndicacao,[contraA1,contraA2]).
relacao(medicamentoH,temContraIndicacao,[contraA3,contraA1]).
relacao(medicamentoK,temContraIndicacao,[contraA3]).

%Alinea 1
ligacaoRecursos(Origem,Destino,Solucao,Qtd):-
	ligacaoRecursos2(Destino,[(0,[Origem])],Solucao,Qtd).

ligacaoRecursos2(Destino,[(Qtd,[Destino|T])|_],Solucao,Qtd):-
	reverse([Destino|T],Solucao).

ligacaoRecursos2(Destino,[(Qtd,[H|T])|Resto],Solucao,QtdSol):-
	findall((C,[X,Q,H|T]),(Destino \== H,relacao(H,Q,Lx),
			       member(X,Lx), \+member(X,[H|T]),
			       C is Qtd+1),Novos),
	append(Resto,Novos,Todos),sort(Todos,Ls),
	ligacaoRecursos2(Destino,Ls,Solucao,QtdSol).

%Alinea 2
repetidos([],[]).
repetidos([H|T],[H|L]):- \+member(H,T),repetidos(T,L).
repetidos([_|T],L):- repetidos(T,L).

%Alinea 3
documentos(ContraIndicacao,LDocs):-
	recursos(documento,LD), recursos(doenca,LDoencas),
	findall(Doc,(member(Doc,LD),
	       ligacaoRecursos(Doc,ContraIndicacao,[Doc,menciona,D|_],_),
	       member(D,LDoencas)),LDoc2),
	repetidos(LDoc2,LDocs).
