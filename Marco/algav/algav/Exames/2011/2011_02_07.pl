/*
Teorica
1.� possivel provar que Z � verdade por refutacao
2.X=a
3.Nenhuma das anteriores
4.� um m�too de pesquisa informado com avaliacoes de transi�oes locais e
que tem em conta a distancia � solucao final
5.00110010
6.Forece todads uma estrutura que facilita o desenvolvimento e
comunica�ao de agentes mesmo que estes nao tenham sido desenvolvidos na
mesma linguagem de programacao
7.

*/
%Teorica 2
p(X):-q(X),r(X).
q(X):-s(X).
s(a). s(b). r(a). r(b). r(c).

%Pratica
%via(No1,No2,Tipo,Tempo)
via(a,b,s,2).
via(a,h,e,10).
via(a,l,s,2).
via(b,c,s,2).
via(b,h,e,10).
via(c,d,e,10).
via(c,g,s,2).
via(c,h,e,10).
via(d,e,e,10).
via(d,g,e,10).
via(e,f,e,10).
via(e,g,s,2).
via(e,i,e,10).
via(e,o,e,10).
via(g,i,e,10).
via(g,j,e,10).
via(g,h,e,10).
via(j,h,e,10).
via(j,i,e,10).
via(l,h,e,10).
via(l,m,s,2).
via(m,j,e,10).
via(m,n,s,2).
via(n,j,e,10).
via(n,o,s,2).
via(o,i,e,10).
via(o,f,s,2).

%Alinea 1
%goa(Origem,Destino,TempoMax,Cam,TCam)
goa(Origem,Destino,TempoMax,Cam,TCam):-
	goa2(Destino,[(0,[Origem])],TempoMax,Cam,TCam).

goa2(Destino,[(TCam,[Destino|T])|_],_,Cam,TCam):-
	reverse([Destino|T],Cam).
goa2(Destino,[(TCam,[H|T])|Resto],TempoMax,Cam,TCamSol):-
	findall((C,[X,H|T]),(Destino\==H,(via(X,H,_,Cx);via(H,X,_,Cx)),\+member(X,[H|T]),C is Cx + TCam, C<TempoMax),Novos),
	append(Novos,Resto,Todos), sort(Todos,LS),
	goa(Destino,LS,TempoMax,Cam,TCamSol).

















