/*
Teorica
1.É possivel provar que ~P∧S é verdade
2.X=[1], Y=[1,2,3],A=[1] e B=[2,3]
3.o Simulated Annealing
4.11001000
5.responde a estimulos dispensando um planeamento mais detalhado
6.56
7.F
*/
%Teorica 2
%X=[1],Y=[1,2,3],append(A,B,Y),A=X,!.

%Pratica
%utilizador(nickname,email,local_atual,lista_amigos).
utilizador(nick0,email0,vilareal,[nick1,nick8]).
utilizador(nick1,email1,lisboa,[nick0,nick3,nick5,nick9]).
utilizador(nick2,email2,porto,[nick5,nick7,nick8]).
utilizador(nick3,email3,braga,[nick1,nick4]).
utilizador(nick4,email4,coimbra,[nick1,nick3,nick2,nick5,nick6]).
utilizador(nick5,email5,porto,[nick1,nick4,nick2]).
utilizador(nick6,email6,lisboa,[nick3,nick2]).
utilizador(nick7,email7,braga,[nick3,nick5]).
utilizador(nick8,email8,porto,[nick3,nick5,nick7]).
utilizador(nick9,email9,vilareal,[nick1,nick4]).

%local(cidadeA,CidadeB,dist) sentido bidireccional
local(lisboa,coimbra,210).
local(braga,porto,55).
local(vilareal,braga,230).
local(coimbra,vilareal,188).
local(porto,coimbra,130).
local(lisboa,faro,280).

%Alinea 1
locais(NU,LT):-

	findall((Local,Total),((local(_,Local,_);local(Local,_,_)),
	       localaux(Lista),locais2(Local,Lista,Total),Total >= NU),
		LC),sort(LC,LT).

localaux(Lista):- findall(Local,utilizador(_,_,Local,_),Lista).

locais2(_,[],0).
locais2(Local,[H|T],Total):-(H == Local,locais2(Local,T,T1),
			     Total is T1 + 1),!;locais2(Local,T,Total).


%Alinea 2
amigosporcidade(Utilizador,LT):-
	utilizador(Utilizador,_,_,LAmigos),
	findall((Local,[H]),(utilizador(H,_,Local,_)
	       ,member(H,LAmigos)),LCA),sort(LCA,LT).

%Alinea 3
caminhos(Utilizador,Caminho,Distancia):-
	utilizador(Utilizador,_,CidadeI,_),
	amigosporcidade(Utilizador,[(_,A)|_]),length(A,Maior),maior([(braga,[nick3]),(porto,[nick5]),(vilareal,[nick0,nick9]),(algarve,[nick8])],CidadeF,Maior),caminhos2(CidadeI,CidadeF,Caminho,Distancia).

maior([],_,_).
maior([(C,A)|T],Cidade,Maior):-length(A,MaiorAux),
	                      ((MaiorAux >= Maior, maior(T,Cidade,MaiorAux),Cidade = C);(maior(T,Cidade,Maior))),!.


caminhos2(CidadeI,CidadeF,Caminho,Distancia):-
	caminhos3(CidadeF,[(0,[CidadeI])],Caminho,Distancia).

caminhos3(CidadeF,[(Distancia,[CidadeF|T])|_],Caminho,Distancia):-
	reverse([CidadeF|T],Caminho).

caminhos3(CidadeF,[(Distancia,[H|T])|Resto],Caminho,DistanciaSol):-
	findall((C,[X,H|T]),(CidadeF \==H,(local(X,H,Cx);local(H,X,Cx)),
	        \+member(X,[H|T]),C is Cx + Distancia),Novos),
	append(Novos,Resto,Todos),sort(Todos,Ls),
         caminhos3(CidadeF,Ls,Caminho,DistanciaSol).
