/*
Teorica
1.� possivel provar que R � verdade por refuta��o
2.D� erro porque o valor de X � diferente do valor de Z
3.Seja G um grafo Cg, se acrescentarmos mais uma opera��o a G teremos
menos planos alternativos do que tinhamos anteriormente
4.o custo da irda orgiem ao n� n
5.11100011 6.as afirmacoes (i) e (iii) s�o ambas verdadeiras
7.X<=3
*/
%Teorica 2
%X=2,Y=[1,2,3],member(Z,Y),!,Z=X.

%Pratica
linha(1,[a,b,c,d,e]).
linha(2,[b,g,f,e,i,h]).
linha(3,[l,m,g,n,f,o,p,q,i,j]).
linha(4,[l,m,g,n,f,o,p,q,i,j]).

%Alinea 1
mesma_linha(Estacao1,Estacao2):- linha(Linha,ListaEst),	write(Linha),nl,
	member(Estacao1,ListaEst), member(Estacao2,ListaEst),
	Estacao1 \== Estacao2,!.

%Alinea 2
muda_linha(Estacao,ListaLinhas,NovaLinha):-
	linha(Linha,ListEstacoes),\+member(Linha,ListaLinhas),
	member(Estacao,ListEstacoes), NovaLinha is Linha.

/*
	muda_linha(Estacao,ListaL,NovaLinha):-
	findall(Id,(linha(Id,ListEst),member(Estacao,ListEst)),ListaL),
	novaLinha(ListaL,ListEst,NovaLinha).

novaLinha([],_,_):-fail.
novaLinha([H|T],ListEst,NL):-
	((\+member(H,ListEst),NL is H, true);novaLinha(T,ListEst,NL)).
*/

%Alinea 3

/*go(Origem,Destino,LE):-
	go1(Destino,[[Origem]],LE).

go1(Destino,[[Destino|T]],LE):-
	reverse([Destino|T],LE).

go1(Destino,[[Destino|T]|Resto],LE):-
	findall([X,H|T],(Destino\==H,linha(_,ListaEst),member(X,ListaEst),\+member(X,[H|T])),Novos),
	append(Novos,Resto,Todos),
	sort(Todos,LS),
	go1(Destino,LS,LE).
*/
