/*
Teorica
1.� possivel provar que C � verdade
2.Nenhuma das anteriores
3.O numero de planos alternativos que existem em G1' � igual ao numero
de planos alternativos existentes em G1
4.o n� de menor custo(obtido atrav�s da distancia estiada ao destino e
do custo para atingir esse n�)
5.Nenhuma das anteriores ?
6.Um agente inteligente deve replicar sempre as ac��es dos sseus
adversarios
7.Nenhuma das anteriores
*/
%Teorica 2
p(X,[X|Ys],Ys).
p(X,[_|Ys],Zs):- p(X,Ys,Zs).

%Pratica
%cidade(Nome_Cidade,Temperatura_Media).
cidade(lisboa,20).
cidade(berlin,10).
cidade(madrid,22).
cidade(londres,7).
cidade(madeira,22).

%voo(CidadeA,CidadeB,horas_Voo,Num_Voo,Companhia)
%Sentido bidirecional
voo(lisboa,londres,2,123,tap).
voo(lisboa,madeira,1.5,124,tap).
voo(berlin,londres,2,323,lufthansa).
voo(berlin,madrid,1.5,421,easyjet).
voo(lisboa,madrid,1,512,iberia).

%companhia(Companhia,tipo)
companhia(tap,regular).
companhia(iberia,regular).
companhia(lufthansa,regular).
companhia(easyjet,'low cost').

%Alinea 1
%cidades(Companhia,Temp_Media,Lista)
cidades(Ca,T,LC):-
	findall(Cidade,((voo(Cidade,_,_,_,Ca);voo(_,Cidade,_,_,Ca)),cidade(Cidade,Temp), Temp >T),LS),sort(LS,LC).

%Alinea 2
%procura_voos(Origem,Destino,SolucaoF,Custo)
procura_voos(Origem,Destino,SolucaoF,Custo):-
	procura_voos2(Origem,Destino,Solucao,Custo),
	num_voo(Solucao,SolucaoF).

procura_voos2(Origem,Destino,Solucao,Custo):-
	procura_voos3(Destino,[(0,[Origem])],Solucao,Custo).

procura_voos3(Destino,[(Custo,[Destino|T])|_],Solucao,Custo):-
	reverse([Destino|T],Solucao).
procura_voos3(Destino,[(Custo,[H|T])|Resto],Solucao,CustoSol):-
	findall((C,[X,H|T]),(Destino \==H,(voo(X,H,Cx,_,_);voo(H,X,Cx,_,_)),\+member(X,[H|T]),C is Cx + Custo),Novos),
	append(Novos,Resto,Todos),
	sort(Todos,LS),
	procura_voos3(Destino,LS,Solucao,CustoSol).

%Transforma a lista de paises para os numeros do voo
%N�o est� a funcionar corretamente
num_voo([],_).
num_voo([H,X|T],R):- (voo(H,X,_,Num,_);voo(X,H,_,Num,_))
                    ,!,num_voo([X|T],[Num|R]).


%Alinea 3
%ligacoes(Origem,Destino,SolucaoF,Custo)
ligacoes(Origem,Destino,SolucaoF,Custo):-
	ligacoes2(Origem,Destino,Solucao,Custo),
	num_voo(Solucao,SolucaoF).

ligacoes2(Origem,Destino,Solucao,Custo):-
	ligacoes3(Destino,[(0,[Origem])],Solucao,Custo).

ligacoes3(Destino,[(Custo,[Destino|T])|_],Solucao,Custo):-
	reverse([Destino|T],Solucao).
ligacoes3(Destino,[(Custo,[H|T])|Resto],Solucao,CustoSol):-
	findall((C,[X,H|T]),(Destino \==H,(voo(X,H,Cx,_,Companhia);voo(H,X,Cx,_,Companhia)),companhia(Companhia,regular),\+member(X,[H|T]),C is Cx + Custo),Novos),
	append(Novos,Resto,Todos),
	sort(Todos,LS),
	ligacoes3(Destino,LS,Solucao,CustoSol).






















