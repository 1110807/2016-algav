/*
Teorica
1.� possivel provar que T � verdade por refuta��o
2.L=[3,2,1]
3.35
4.DGKNI
5.A probabilidade de selecao de individuos pode n�o ser igual para todos
6.Um sistema composto por dois ou mais agentes inteligentes que s�o
capazes de coordenar os seus objetivos e conhecimentos visando a
resolucao de um determinado problema
7.(9!)^2
*/
%Teorica 2
p([],[]).
p([H|T],R):-p(T,TR),append(TR,[H],R).

%Pr�tica
utilizador(pedro,estudante).
utilizador(jo�o,designer).
utilizador(joaquim,inform�tico).
app('angry birds',jogo).
app('fruit ninja',jogo).
app(ebuddy,social).
app('Adobe Reader',produtividade).
amigo(pedro,jo�o).
amigo(joaquim,jo�o).
recomenda(pedro,'Acrobat Reader').
recomenda(jo�o,ebuddy).
recomenda(jo�o,'Acrobat Reader').
instalado(pedro,'angry birds').
instalado(jo�o,ebuddy).
instalado(joaquim,'fruit ninja').
instalado(jo�o,'angry birds').

%Alinea 1
recomendados(UserX,Categoria,AppsRec):- findall(Nome,(utilizador(UserX,_),app(Nome,Categoria),recomenda(UserX,Nome)),AppsRec).

%Alinea 2
%ligado(UserX,UserY,Path)
ligado(Origem,Destino,Caminho):-
	ligado1(Destino,[[Origem]],Caminho).

ligado1(Destino,[[Destino|T]|_],Caminho):-
	reverse([Destino|T],Caminho).
ligado1(Destino,[[H|T]|Outros],Caminho):-
	findall([X,H|T],(Destino\==H,(amigo(X,H);amigo(H,X)), \+member(X,[H|T])),Novos), append(Outros,Novos,Todos),
	ligado1(Destino,Todos,Caminho).

%Alinea 3
