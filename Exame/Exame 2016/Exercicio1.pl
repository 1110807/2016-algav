%aluno(numero, unidadecurricular, momento, avaliacao, nota)%
aluno(1, algav,1,10).
aluno(1, algav,2,7).
aluno(1, algav,3,6).
aluno(2, algav,1,11).
aluno(2, algav,2,10).
aluno(2, algav,3,13).
aluno(3, algav,1,5).
aluno(3, algav,2,6).
aluno(3, algav,3,4).

%componente(momento avaliacao, peso na nota)%
componente(1,0.2).
componente(2,0.2).
componente(3,0.6).

aluno_sem_minimo(N,C,P):-
	aluno(N,C,M,V),
	componente(M,P),
	P > 0.5,
	V < 8,
	write(N), write(' sem m�nimo a '), write(C), nl,
	fail; true.
