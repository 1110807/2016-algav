%Criar o predicado agrupa_replicados[Lista1, Lista2] que dada uma Lista0
% com sequencias de elementos repetidos, controi a lista Lista1 de
% sublistas contendo cada uma das sequencias de elementos. Por exemplo
% ? - agrupa_replicados([a,b,b,b,c,c,c,c],Lista1).
% Lista 1 = [[a],[b,b,b],[c,c,c,c]]

/*
pack([],[]).
pack([X|L1],[Z|LF]) :-
	transfer(X,L1,L2,Z),
	pack(L2,LF).

transfer(X,[],[],[X]).

transfer(X,[Y|L2],[Y|L2],[X]) :-
	X \= Y.

transfer(X,[X|L1],L2,[X|LF]) :-
	transfer(X,L1,L2,LF).


agrupa_replicados([],[]).

agrupa_replicados([X|L1],[Y,X|Lf]):-
	X == Y,
	agrupa_replicados(L1,[X,Y|Lf]).

agrupa_replicados([X|L1],[[X]|Lf]):-
	agrupa_replicados(L1,Lf).
*/

agrupa_replicados([],[]).

agrupa_replicados([X|L1],[Z|Lf]):-
	agrupa_replicados2(X,L1,L2,Z),
	agrupa_replicados(L2,Lf).

agrupa_replicados2(X,[],[],X).

agrupa_replicados2(X,[Y|L2], L2,[X|Lf])
