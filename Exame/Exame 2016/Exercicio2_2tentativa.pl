/*
agrupa_replicados([a,b,b,b,c,c,c,c],ListaFinal).
ListaFinal = [[a],[b,b,b],[c,c,c,c]]
*/

agrupa_replicados([],[]).

agrupa_replicados([X|L1],[Z|LF]) :-
	agrupa_replicados2(X,L1,L2,Z),
	agrupa_replicados(L2,LF).

agrupa_replicados2(X,[],[],[X]).

agrupa_replicados2(X,[Y|L2],[Y|L2],[X]):-
	X\=Y.

agrupa_replicados2(X,[X|L1],L2,[X|LF]):- agrupa_replicados2(X,L1,L2,LF).

