/*
1 - Criar o predicado adjacente_diferenca(Lista,Val,R) que dada uma
Lista de inteiros e um valor Val, devolva em R (por backtracking) os
elementos cuja diferen�a para cada um dos seus adjacentes � igual a Val.
Por exemplo:

?- adjacente_diferenca([1,4,5,3,8,7,5,3,1],2,R).
R = 5;
R = 3;
R = 1;
false.

2 - Criar o predicado todos_adj_dif (ListadeListas,Val,ListaResultado)
que dadas uma lista de listas inteiros (ListadeListas) e um valor
inteiro Val, devolve em ListaResultado, todos os elementos dessas listas
cuja diferen�a para cada um dos seus adjacentes � igual a Val. Pode
assumir que o predicado da pergunta anterior se encontra implementado.
Por exemplo

diff(X,Y,Val):-
	P is abs(X-Y),
	Val == P.

adj([],_,_).
adj([X,Y|Lista],Val,R):-
	diff(X,Y,Val),
	adj(Lista,Val,R).


adj(Lista,Val,R):-
	adj(Lista,Val,R).
*/
calc_diff(A,B, Diff):-
	D is abs(A-B),
	D == Diff.

adj(Lista, Diff, R):-
	adj(Lista, Diff, R, 1).

adj([V],_,V,1).
adj([P,V|T], Dif,R,0):-
	calc_diff(P,V,Dif),!,
	adj([V|T], Dif,R,1); !,
	adj([V|T], Dif, R, 0).

adj([P,V|_], Dif, P, 1):-
	calc_diff(P,V,Dif).

adj([P,V|T],Dif,R,1):-
	calc_diff(P,V,Dif), !,
	adj([V|T], Dif, R, 1);
	adj([V|T],Dif,R,0).

