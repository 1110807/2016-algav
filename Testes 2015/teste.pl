%professor(Nome,Turma)
professor('Israel Izidro','2na').
professor('Israel Izidro','2nb').
professor('Sabina Simoes','2da').
professor('Feliciano Faria','2db').
professor('Feliciano Faria','2dc').

%aluno(Nome,Turma)
aluno(1,'Marcia Marques','2na').
aluno(2,'Mario Madureira','2da').
aluno(3,'Antonio Antunes','2db').
aluno(4,'Luis Lima','2db').
aluno(5,'Bernardino Bezerra','2na').
aluno(6,'Carlos Costa','2na').
aluno(7,'Carlos Costa','2nc').
aluno(8,'Pedro Pereira','2da').
aluno(9,'Tadeu Torres','2da').
aluno(10,'Zuleica Zeferino','2db').
aluno(11,'Davide Gomes','2nb').

turmaSemAlunos(X):-
	professor(_,X),
	\+(aluno(_,_,X)).

/*
mostrarGrupos(X):-
	professor(X,Z),
	write(X),write(","),write(Z),
	nl,
	mostrarGrupos(X),
	aluno(_,Y,Z),
	write(Y).

== Sem recursividade ==
mostrarGrupos(X):-
	professor(X,Z),!,
	aluno(_,Y,Z),
	write(Y),
	nl,
	fail;true.
*/

mostrarGrupos(X):-
	professor(X,Z),!,
	write(X),write(","),write(Z),
	nl,
	mostrarGrupos(Z),
	aluno(_,Y,Z),
	write(Y), write(','), write(Z),
	nl,
	fail;true.
